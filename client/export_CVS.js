exportCVS = function() {
	// clientLog ('export CVS');
	data = [];

	// Conference ///////////////////////////////////////////////////////////////////////////////

	data.push('Conference;');
	data.push("\n");

	data.push('Dag;');
	data.push('Tijd;');
	data.push('Naam;');
	data.push('Email;');
	data.push('Telefoon;');
	data.push('Bedrijf;');
	data.push('Functie;');
	data.push('Nationaliteit;');
	data.push("\n");

	var array = Registrations.find({track: 'conference'}, {sort: {timeStamp: -1}}).fetch();
	for(var i = 0; i < array.length; i++) {
		reg = array[i];
		data.push( reg.timeTextDay + ';');
		data.push( reg.timeTextTime + ';');
		data.push( reg.naam + ';');
		data.push( reg.email + ';');
		data.push( reg.telefoon + ';');
		data.push( reg.bedrijf + ';');
		data.push( reg.functie+ ';');
		data.push( reg.nationaliteit + ';');
		data.push("\n");
	}
	data.push("\n");

	// Awareness ///////////////////////////////////////////////////////////////////////////////

	data.push('Awareness;');
	data.push("\n");

	data.push('Dag;');
	data.push('Tijd;');
	data.push('Naam;');
	data.push('Email;');
	data.push('Telefoon;');
	data.push('Sessie;');
	data.push("\n");

	var array = Registrations.find({track: 'awareness'}, {sort: {sessionName: -1}}).fetch();
	for(var i = 0; i < array.length; i++) {
		reg = array[i];
		data.push( reg.timeTextDay + ';');
		data.push( reg.timeTextTime + ';');
		data.push( reg.naam + ';');
		data.push( reg.email + ';');
		data.push( reg.telefoon + ';');
		data.push( reg.sessionName + ';');
		data.push("\n");
	}

	var blob = new Blob(data, {type:'text/csv'});
	saveAs(blob, 'Think Dementia registrations.csv');
	return('CVS exported');

}
