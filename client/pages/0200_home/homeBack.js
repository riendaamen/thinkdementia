
// home ////////////////////////////////////////////////////

Template.homeCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsHomePage', 				name: 'Page'},
			{component: 'cmsHomeSlider', 			name: 'Slider'}
		];
	}
});


Template.cmsHomePage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'pageText',		name: 'Title'},
			{tabName: 'seo', 				name: 'SEO'}
		];
	}
});


Template.cmsHomeSlider.helpers({
	group: function() {
		Session.set('currentPostGroup', 'slider');
	},
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'homeSlider',	name: 'Slide'}
		];
	}
});
