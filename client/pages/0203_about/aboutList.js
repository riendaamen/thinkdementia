// aboutList ////////////////////////////////////////////////////////

Template.aboutList.helpers({
	show: function() {
		return ( getPageSlugBase() == 'over-het-evenement.path-nl' ) ? true : false;
	},
	item: function() {
		var l = Session.get('language');
		var p = 'over-het-evenement.path-nl';
		var g = '_pagesGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}});
		return query;
	},

	classSelected: function() {
		return (Session.get('currentAbout') == this._id) ? 'active' : '';
	},

	baseURL: function() {
		return(getPageName(2, 0, false));
	}
});

Template.aboutList.events({
	'click .aboutBtn': function(e){																// goto link
		e.preventDefault();
		var routerName = getPageName(3, 0, false);
		Session.set('currentAbout', this._id);
		clientLog ('routerName = ' + routerName + this.slug);
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});
