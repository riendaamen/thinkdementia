// about  /main 		////////////////////////////////////////////////////////

Template.about.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	},
	
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		// var g = getPostGroupSlug();
		var g = '_pagesGroup';
		var id = Posts.find( {_id: Session.get('currentAbout'), page: p, groupSlug: g, language: l} );
		if (id.count() == 0){ 		// No result
			id = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 1} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentAbout', id.fetch()[0]._id);
		}
		return id;
	}
	
});

Template.about.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});








// about  /main/x 		////////////////////////////////////////////////////////

Template.about_slug1.helpers({
	// item: function() {
		// var l = Session.get('language');
		// var p = getPageSlugBase();
		// var g = '_pagesGroup';
		// var s = Session.get('navParam1');
		// var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		// getSEO_fromPost(query);
		// return query;
	// }
	
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		// var g = getPostGroupSlug();
		var g = '_pagesGroup';
		var id = Posts.find( {_id: Session.get('currentAbout'), page: p, groupSlug: g, language: l} );
		if (id.count() == 0){ 		// No result
			id = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 1} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentAbout', id.fetch()[0]._id);
		}
		return id;
	}

	
});

Template.about_slug1.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName);
	}
});








