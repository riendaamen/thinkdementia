// Program ////////////////////////////////////////////////////

Template.programCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsProgramPage', 					name: 'Page'},
			{component: 'cmsProgramTrack', 					name: 'Track'}
		];
	}
});


Template.cmsProgramPage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'pageText', name: 'Text'},
			{tabName: 'seo', 			name: 'SEO'}
		];
	}
});


Template.cmsProgramTrack.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'programDescription2', name: 'Description'},
			{tabName: 'seo', 								name: 'SEO'}
		];
	}
});
