// symposium  /main 		////////////////////////////////////////////////////////

Template.symposium.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}).fetch();
		var list = [];
		for(var i = 0; i < query.length; i++) {
			var item = {};
			var q = query[i];
			item.title = 			q.title;
			item.screenText = q.screenText;
			item.slug = 			q.slug;
			var start = 			(((q.startHour*60) + q.startQuarter) * 1.5);
			var end = 				(((q.endHour*60) + q.endQuarter) * 1.5);
			item.start = 			start;
			item.end = 				end;
			item.top = 				start-770;		// *1 540  *1.33 720   *1.5 810    *2 1080
			item.height = 		(end-start)-2;
			item.trackType = 	q.trackType;
			item.trackPos = 	'';
			if (q.trackPos==1) {item.trackPos = 'leftTrack'}
			if (q.trackPos==2) {item.trackPos = 'rightTrack'}
			list.push(item);
		}
		return list;
	}
});

Template.symposium.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// col Left ////////////////////////////////////////////////////////

Template.symposiumLeftCol.rendered = function() {
	renderCufont();
}

Template.symposiumLeftCol.helpers({
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});



// symposium_slug1  /main/x 		////////////////////////////////////////////////////////

Template.symposium_slug1.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = nav.param1;

		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);

		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s}).fetch();
		var list = [];
		for(var i = 0; i < query.length; i++) {
			var item = {};
			var q = query[i];
			item.title = 			q.title;
			item.screenText = q.screenText;
			// item.slug = 			q.slug;
			item.article = 		q.article;
			// var start = 			(((q.startHour*60) + q.startQuarter) * 1.5);
			// var end = 				(((q.endHour*60) + q.endQuarter) * 1.5);
			// item.start = 			start;
			// item.end = 				end;
			// item.top = 				start-810;		// *1 540  *1.33 720   *1.5 810    *2 1080
			// item.height = 		(end-start)-2;
			item.trackType = 	q.trackType;
			// item.trackPos = 	'';
			// if (q.trackPos==1) {item.trackPos = 'leftTrack'}
			// if (q.trackPos==2) {item.trackPos = 'rightTrack'}
			list.push(item);
		}
		return list;
	}
});
