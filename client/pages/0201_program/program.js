// program  /main 		////////////////////////////////////////////////////////

Template.program.helpers({
	tracks: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g});
		return query;
	}
});

Template.program.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// left column 		////////////////////////////////////////////////////////

Template.programLeftCol.rendered = function() {
	renderCufont();
}

Template.programLeftCol.helpers({
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});




// bewustwording_slug1  /main/x 		////////////////////////////////////////////////////////

Template.program_slug1.helpers({
	track: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = Session.get('navParam1');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});


