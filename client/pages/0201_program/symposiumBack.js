// Symposium ////////////////////////////////////////////////////

Template.symposiumCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsSymposiumRegistration', name: 'Registrations'},
			{component: 'cmsSymposiumPage', 				name: 'Page'},
			{component: 'cmsSymposiumTrack', 				name: 'Track'}
		];
	}
});



Template.cmsSymposiumPage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'pageText', name: 'Text'},
			{tabName: 'seo', 			name: 'SEO'}
		];
	}
});

Template.cmsSymposiumTrack.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'symposiumTitle', 		name: 'Title'},
			{tabName: 'programDescription', name: 'Description'},
			{tabName: 'seo', 								name: 'SEO'}
		];
	}
});

Template.cmsSymposiumRegistration.helpers({
});
