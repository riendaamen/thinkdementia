// Bewustwording ////////////////////////////////////////////////////

Template.bewustwordingCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsBewustwordingRegistration', 	name: 'Registrations'},
			{component: 'cmsBewustwordingPage', 					name: 'Page'},
			{component: 'cmsBewustwordingTrack', 					name: 'Track'}
		];
	}
});



Template.cmsBewustwordingPage.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'pageText', name: 'Text'},
			{tabName: 'seo', 			name: 'SEO'}
		];
	}
});

Template.cmsBewustwordingTrack.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'bewustwordingTitle', name: 'Title'},
			{tabName: 'programDescription', name: 'Description'},
			{tabName: 'seo', 								name: 'SEO'}
		];
	}
});

Template.cmsBewustwordingRegistration.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'bewustwordingRegs', name: 'Registrations'}
		];
	}
});
