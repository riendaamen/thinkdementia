// newsList short ////////////////////////////////////////////////////////

Template.newsListX3.helpers({
	item: function() {
		var l = Session.get('language');
		var p = 'nieuws.path-nl'; 
		var g = '_pagesGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}, limit: 3});
		return query;
	},
	baseURL: function() {
		return(getPageName(2, 0, false));
	}
});

Template.newsListX3.events({
	'click .newsListBtn': function(e){											// goto link
		e.preventDefault();
		var routerName = getPageName(2, 0, false);
		clientLog ('routerName = ' + routerName + this.slug);
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// newslist long//////////////////////////////////////////////////

Template.newsListX5.helpers({
	item: function() {
		var l = Session.get('language');
		var p = 'nieuws.path-nl'; 
		var g = '_pagesGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}, limit: 9});
		return query;
	},
	baseURL: function() {
		return(getPageName(2, 0, false));
	}
});

Template.newsListX5.events({
	'click .newsListBtn': function(e){											// goto link
		e.preventDefault();
		var routerName = getPageName(2, 0, false);
		clientLog ('routerName = ' + routerName + this.slug);
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});




