// News ////////////////////////////////////////////////////

Template.newsCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsNewsPosts', name: 'Posts'}
		];
	}
});


Template.cmsNewsPosts.helpers({
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'newsShort', 	name: 'Short'},
			{tabName: 'newsLong', 	name: 'Long'},
			{tabName: 'seo', 				name: 'SEO'}
		];
	}
});

