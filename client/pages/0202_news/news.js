// news  /main 		////////////////////////////////////////////////////////


Template.news.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},
	item: function() {
		var l = Session.get('language');
		var p = 'nieuws.path-nl';
		var g = '_pagesGroup';
		// var s = '_main';
		// var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}});
		//getSEO_fromPost(query);
		return query;
	},
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	},
	thumb: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('language');
		// if ( getPageSlugBase() == 'actueel.agenda' ) {
			// l = -1;
		// }
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.news.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// news  /main/x 		////////////////////////////////////////////////////////


Template.news_slug1.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = Session.get('navParam1');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);

		query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s}).fetch();
		var list = [];
		for(var i = 0; i < query.length; i++) {
			var item = {};
			var q = query[i];
			item.title = 			q.title;
			item.dateNice = 	q.dateNice;
			item.newsLong = 	q.newsLong;
			item.newsShort = 	q.newsShort;

			item.author = 				'Think Dementia!';
			item.excerpt = 				q.newsShort;
			item.article = 				q.newsShort;
			item.description  = 	q.newsShort;
			item.summary  = 			q.newsShort;
			list.push(item);
		}
		return list;
	}

});
