// home text //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_pageText.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_pageText.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':				$('#input_title').val(),
				'homeTextL':			CKEDITOR.instances.input_homeTextL.getData()
				// 'homeTextR':			CKEDITOR.instances.input_homeTextR.getData()
			}});
			Session.set('edited', false);
		}
	}
});

