// Symposium Registration //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_bewustwordingRegs.helpers({
	countTotal: function() {
		return Registrations.find({sessionId: this._id}, {sort: {timeStamp: -1}}).fetch().length;
	},
	item: function() {
		return Registrations.find({sessionId: this._id}, {sort: {timeStamp: -1}});
	}
});


Template.cmsEdit_bewustwordingRegs.events({
	'click .delRegBtn': function(e) {															// delete
		var conf = confirm("Deze registratie verwijderen?");
		if (conf){ Registrations.remove( {_id: this._id} ); }
	},
	'click #exportCVS': function(e) {															// Export CVS
		exportCVS();
	},
	'click #input_openReg': function(e) {													// open/close reg
		clientLog('openReg on/off');
		var vis = (document.getElementById('input_openReg').checked) ? 'checked' : '';
		Posts.update( {'_id': this._id}, {$set: {
			'openReg': vis
		}});
	}
});
