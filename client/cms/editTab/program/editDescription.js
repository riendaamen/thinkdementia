// Program description //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_programDescription.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_programDescription.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'article':			CKEDITOR.instances.input_article.getData()
			}});
			Session.set('edited', false);
		}
	}
});




// Program description 2 //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_programDescription2.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_programDescription2.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':				$('#input_title').val(),
				'article':			CKEDITOR.instances.input_article.getData()
			}});
			Session.set('edited', false);
		}
	}
});

