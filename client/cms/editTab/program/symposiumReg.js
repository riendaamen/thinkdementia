// Symposium Registration //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_symposiumRegistration.helpers({
	item: function() {
		return Registrations.find({track: 'conference'}, {sort: {timeStamp: -1}});
	},
	countTotal: function() {
		return Registrations.find({track: 'conference'}).fetch().length;
	},
	list: function() {
		Session.set('currentPostGroup', '_mainGroup');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var main = Posts.find({'language': 0, 'page': p, 'groupSlug': g, 'slug': '_main'});
		return main;
	}
});


Template.cmsEdit_symposiumRegistration.events({
	'click .delRegBtn': function(e) {															// delete
		var conf = confirm("Deze registratie verwijderen?");
		if (conf){ Registrations.remove( {_id: this._id} ); }
	},
	'click #input_openReg': function(e) {													// open/close reg
		clientLog('openReg on/off');
		var vis = (document.getElementById('input_openReg').checked) ? 'checked' : '';
		Posts.update( {'_id': this._id}, {$set: {
			'openReg': vis
		}});
	},
	'click #exportCVS': function(e) {															// export CVS
		exportCVS();
	}
});
