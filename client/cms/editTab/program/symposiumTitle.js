// symposium title //////////////////////////////////////////////////////////////////////////

// Track type (Local Collection)
trackType = new Mongo.Collection(null);
trackType.insert({index: 0, name: ['Algemeen', 						'General']});
trackType.insert({index: 1, name: ['Plenaire sessie',			'Plenary session']});
trackType.insert({index: 2, name: ['Living labs',					'Living labs']});
trackType.insert({index: 3, name: ['Markt implementatie',	'Market implemantation']});

// Track pos (Local Collection)
trackPos = new Mongo.Collection(null);
trackPos.insert({index: 0, name: 'Both'});
trackPos.insert({index: 1, name: 'Left'});
trackPos.insert({index: 2, name: 'Right'});

Template.cmsEdit_symposiumTitle.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	},
	startHours: function() {
		var slides = [];
		for (var i = 9; i < 19; i++) {
			var slide = [];
			slide.id = this._id;
			slide.number = i;
			slide.text = i.toString();
			if (slide.text.length == 1) {slide.text = '0' + slide.text;}
			slide.active = (i == this.startHour) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	startQuarters: function() {
		var slides = [];
		for (var i = 0; i < 4; i++) {
			var slide = [];
			slide.id = this._id;
			slide.number = i*15;
			slide.text = (i*15).toString();
			if (slide.text.length == 1) {slide.text = '0' + slide.text;}
			slide.active = (i*15 == this.startQuarter) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	endHours: function() {
		var slides = [];
		for (var i = 9; i < 19; i++) {
			var slide = [];
			slide.id = this._id;
			slide.number = i;
			slide.text = i.toString();
			if (slide.text.length == 1) {slide.text = '0' + slide.text;}
			slide.active = (i == this.endHour) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	endQuarters: function() {
		var slides = [];
		for (var i = 0; i < 4; i++) {
			var slide = [];
			slide.id = this._id;
			slide.number = i*15;
			slide.text = (i*15).toString();
			if (slide.text.length == 1) {slide.text = '0' + slide.text;}
			slide.active = (i*15 == this.endQuarter) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	trackTypes: function() {
		var query = trackType.find({}).fetch();
		var slides = [];
		var language = Session.get('cmsLanguage');
		for (var i = 0; i < query.length; i++) {
			var slide = [];
			slide.index = i;
			slide.id = this._id;
			slide.name = query[i].name[language];
			slide.color = 'color' + i;
			slide.selected = (i == this.trackType) ? 'selected' : '';
			slides.push(slide);
		}
		return slides;
	},
	trackPos: function() {
		var query = trackPos.find({}).fetch();
		var slides = [];
		for (var i = 0; i < query.length; i++) {
			var slide = [];
			slide.index = i;
			slide.id = this._id;
			slide.name = query[i].name;
			slide.selected = (i == this.trackPos) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	}

});




Template.cmsEdit_symposiumTitle.events({
	'click #update': function(e) {															// Update
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':				$('#input_title').val(),
				'screenText':		$('#input_screenText').val()
			}});
			Session.set('edited', false);
		}
	},
	'click .btnStartHour': function(e) {						// select start hour
		setTime('start', 60, this.number, this.id);
	},
	'click .btnStartQuarter': function(e) {					// select start quarter
		setTime('start', 15, this.number, this.id);
	},
	'click .btnEndHour': function(e) {							// select end hour
		setTime('end', 60, this.number, this.id);
	},
	'click .btnEndQuarter': function(e) {						// select end quarter
		setTime('end', 15, this.number, this.id);
	},
	'click .btnTrackType': function(e) {						// select track type
		// clientLog('select color ' + this.index);
		Posts.update( {'_id': this.id}, {$set: {
			'trackType': this.index
		}});
	},
	'click .btnTrackPos': function(e) {						// select track pos
		// clientLog('select pos ' + this.index);
		Posts.update( {'_id': this.id}, {$set: {
			'trackPos': this.index
		}});
	}
});



setTime = function(entry, mult, value, id) {		// start|end 	60|15
	var post = Posts.find({'_id': id}).fetch()[0];
	var startHour			= post.startHour;
	var startQuarter	= post.startQuarter;
	var endHour				= post.endHour;
	var endQuarter		= post.endQuarter;

	var start	=	(startHour*60) + 	(startQuarter);
	var end		=	(endHour*60) + 		(endQuarter);

	if (entry=='start' && mult==60) {start =	(value*60) + 			(startQuarter);}
	if (entry=='start' && mult==15) {start =	(startHour*60) + 	(value);}
	if (entry=='end' 	&& mult==60) 	{end =		(value*60) + 			(endQuarter);}
	if (entry=='end' 	&& mult==15) 	{end =		(endHour*60) + 		(value);}

	if (start > 1110) {start = 1110; }
	if (end < 555) {end = 555; }

	if (entry=='start' && start >= end) 	{end = start + 15; }
	if (entry=='end' && end <= start) 		{start = end - 15; }

	Posts.update( {'_id': id}, {$set: {
		'startHour': 		Math.floor(start/60),
		'startQuarter': start - (Math.floor(start/60)*60),
		'endHour': 			Math.floor(end/60),
		'endQuarter': 	end - (Math.floor(end/60)*60)
	}});

}
