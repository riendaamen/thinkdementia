// carousel //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_carousel.helpers({
	image: function() {
		return ImagesCarousel.find( {'_id': this.image_01 } );
	}
});

Template.cmsEdit_imageUpload_carousel.events({
  'change #fileUploader': function(event, template) {
		ImagesCarousel.remove( {'_id': this.image_01 } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesCarousel.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image_01':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesCarousel.remove( {'_id': this._id} );
		}
	}
});



// thumb //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_thumb.helpers({
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.cmsEdit_imageUpload_thumb.events({
  'change #fileUploader': function(event, template) {
		// ImagesCarousel.remove( {'_id': this.image_01 } );
		ImagesThumb.remove( {'_id': this.image_01 } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesThumb.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image_01':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesThumb.remove( {_id:this._id} );
		}
	}
});



// photo maurice //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_photo_maurice.helpers({
	image: function() {
		return ImagesPhoto.find( {'_id': this.image_maurice } );
	}
});

Template.cmsEdit_imageUpload_photo_maurice.events({
  'change #fileUploader': function(event, template) {
		ImagesPhoto.remove( {'_id': this.image_maurice } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesPhoto.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image_maurice':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesPhoto.remove( {_id:this._id} );
		}
	}
});



// photo anke //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_photo_anke.helpers({
	image: function() {
		return ImagesPhoto.find( {'_id': this.image_anke } );
	}
});

Template.cmsEdit_imageUpload_photo_anke.events({
  'change #fileUploader': function(event, template) {
		ImagesPhoto.remove( {'_id': this.image_anke } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesPhoto.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image_anke':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesPhoto.remove( {_id:this._id} );
		}
	}
});

// photo contact //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_photo_contact.helpers({
	image: function() {
		return ImagesPhoto.find( {'_id': this.image_contact } );
	}
});

Template.cmsEdit_imageUpload_photo_contact.events({
  'change #fileUploader': function(event, template) {
		ImagesPhoto.remove( {'_id': this.image_contact } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesPhoto.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image_contact':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesPhoto.remove( {_id:this._id} );
		}
	}
});



// customer //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_imageUpload_customer.helpers({
	image: function() {
		return ImagesCustomer.find( {'_id': this.image } );
	}
});

Template.cmsEdit_imageUpload_customer.events({
  'change #fileUploader': function(event, template) {
		ImagesCustomer.remove( {'_id': this.image } );
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesCustomer.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			ImagesCustomer.remove( {_id:this._id} );
		}
	}
});
