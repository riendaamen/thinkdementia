// news short //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_newsShort.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_newsShort.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':				$('#input_title').val(),
				'newsShort':		CKEDITOR.instances.input_newsShort.getData()
			}});
			Session.set('edited', false);
		}
	}
});


// news long //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_newsLong.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_newsLong.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'newsLong':			CKEDITOR.instances.input_newsLong.getData()
			}});
			Session.set('edited', false);
		}
	}
});

