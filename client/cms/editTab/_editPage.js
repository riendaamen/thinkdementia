
// edit Page switcher ////////////////////////////////////////////////////

Session.set('navEditTab', 0);

Template.editPage.helpers({
	list: function() {
		var list = [];
		var tabs = nav.editTabs;
		for(var i = 0; i < tabs.length; i++) {
			var item = {};
			item.num = i;
			item.name = tabs[i].name;
			item.tabName = tabs[i].tabName;
			list.push(item);
		}
		return list;
	},
	listSelected: function() {
		return Session.equals('navEditTab', this.num) ? 'active' : '';
	},
	currentTab: function() {
		return nav.editTabs[Session.get('navEditTab')].tabName;
	},
	item: function() {
		Session.set('edited', false);
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var id = Posts.find( {_id: Session.get('currentId'), page: p, groupSlug: g, language: l} );
		if (id.count() == 0){ 		// No result
			id = Posts.find( {page: p, groupSlug: g, language: l}, {sort: {order: 1}, limit: 1} );
		}
		if (id.count() > 0){ 			// Pick first result
			Session.set('currentId', id.fetch()[0]._id);
		}
		return id;
	}
});



Template.editPage.events({
	'click .listBtn': function(e){															// Select editTab
		e.preventDefault();
		var level1 = Session.get('navLevel1');
		var level2 = Session.get('navLevel2');
		var cmsTab = Session.get('navCmsTab');
		nav.editLast[level1][level2][cmsTab] = this.num;
		Session.set('navEditTab', this.num);
		Session.set('edited', false);
	},
	'keydown #main-item': function(e) {
		Session.set('edited', true);
	},
	'click .checkbox' : function(e) {
		Session.set('edited', true);
	}
});
