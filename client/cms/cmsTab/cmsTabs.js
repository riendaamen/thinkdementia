Template.cmsTabs.helpers({
	tabs: function() {
		var array = nav.cmsTabs;
		var list = [];
		for(var i = 0; i < array.length; i++) {
			var item = {};
			item.index = i;
			item.name = array[i].name;
			var sel = (i == Session.get('navCmsTab')) ? 'active' : 'qq';
			item.classSelected = sel;
			list.push(item);
		}
		return list;
	},
	pageID: function() {
		return getPageIndex();
	}
});

Template.cmsTabs.events({
	'click .btn': function(e) {			// Choose nav cms tab
		e.preventDefault();
		var level1 = Session.get('navLevel1');
		var level2 = Session.get('navLevel2');
		nav.cmsLast[level1][level2] = this.index;
		Session.set('navCmsTab', this.index);

		// set editTab
		var lastEdit = nav.editLast[level1][level2][this.index];
		Session.set('navEditTab', lastEdit);
	}
});
