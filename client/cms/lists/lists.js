// List A ///////////////////////////////////////////////////////////////////////////////////
// 					main page, 				/main
// 					grp = _mainGroup
// 					slg = _main
Session.set('currentPostGroup', 0);			// groupSlug
Session.set('addDialogShow', false);
Session.set('currentId', 0);						// otherwise del btn is active in pages2

Template.cmsListA.helpers({
	list: function() {
		Session.set('currentPostGroup', '_mainGroup');
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var main = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': '_main'});
		return main;
	},
	postFound: function() {
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': '_main'});
		var ret = (post.count() == 1) ? true : false;
		return ret;
	}
});

Template.cmsListA.events({
	'click #tempAdd': function(){											// Add post
		var l = Session.get('cmsLanguage');
		addPost('_main', 'Main title', l);
	},
	'click #tempDel': function(){											// Delete
		Posts.remove( {_id: this._id} );
	}
});



// List B1 ///////////////////////////////////////////////////////////////////////////////////
// 					singleSlug, 				/main/item
// 					grp = _pagesGroup
// 					slg = ~
Template.cmsListB.helpers({
	list: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('cmsLanguage');
		if ( getPageSlugBase() == 'actueel.agenda' ) {
			l = -1;
		}
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	listSelected: function() {
		return Session.equals('currentId', this._id) ? 'active' : '';
	},
	deleteStatus: function() {
		var classActive = 'enabled';
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, '_id': Session.get('currentId')} );
		if (query.count() == 0){ 		// If no posts, disable delete button
			classActive = 'disabled';
		}
		return {buttonId: Session.get('currentId'), classActive: classActive};
	},
	addDialogShow: function() {
		return Session.get('addDialogShow');
	},
	upDownShow: function() {
		return ( getPageSlugBase() == 'actueel.agenda' ) ? false: true;
	},
	moveSatus: function() {
		return {moveId: Session.get('currentId')};
	}
});

Template.cmsListB.events({
	'click #listAdd': function(){											// Add post
		Session.set('addDialogShow', true);
	},
	'click #listDelete': function(){									// Delete
		if (this.classActive == 'enabled') {
			var r = confirm("Deze pagina verwijderen?");
			if (r == true){
				Posts.remove( {_id: this.buttonId} );
			}
		}
	},
	'click .listBtn': function(){											// Select in list
		Session.set('currentId', this._id);
	},
	'click #btnListUp': function(){										// Move up
		listMove(this.moveId, -15);
	},
	'click #btnListDown': function(){									// Move down
		listMove(this.moveId, +15);
	}
});



// List B2 ///////////////////////////////////////////////////////////////////////////////////
// 					doubleSlug,					/main/group/item
// 					grp = ~
// 					slg = ~
Template.cmsListB2.helpers({
	list: function() {
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var grpId = Session.get('currentBlogGroupId');
		var grp = Posts.find( {'_id': grpId} );
		if (grp.count() == 0) {
			grp = Posts.find({'language': l, 'page': p, 'groupSlug': '_groupsGroup'}, {sort: {order: 1}} );
		}
		if (grp.count() > 0) {
			var grpName = grp.fetch()[0].slug;
			Session.set('currentPostGroup', grpName);
			var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
			Session.set('currentBlogGroupId', grp.fetch()[0]._id)
		}
		return item;
	},
	listSelected: function() {
		return Session.equals('currentId', this._id) ? 'active' : '';
	},
	deleteStatus: function() {
		var classActive = 'enabled';
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, '_id': Session.get('currentId')} );
		if (query.count() == 0){ 		// If no posts, disable delete button
			classActive = 'disabled';
		}
		return {buttonId: Session.get('currentId'), classActive: classActive};
	},
	addDialogShow: function() {
		return Session.get('addDialogShow');
	},
	upDownShow: function() {
		return ( getPageSlugBase() == 'actueel.agenda' ) ? false: true;
	},
	moveSatus: function() {
		return {moveId: Session.get('currentId')};
	}
});

Template.cmsListB2.events({
	'click #listAdd': function(){											// Add post
		Session.set('addDialogShow', true);
	},
	'click #listDelete': function(){									// Delete
		if (this.classActive == 'enabled') {
			var r = confirm("Deze pagina verwijderen?");
			if (r == true){
				Posts.remove( {_id: this.buttonId} );
			}
		}
	},
	'click .listBtn': function(){											// Select
		// clientLog('select page');
		Session.set('currentId', this._id);
	},
	'click #btnListUp': function(){										// Move up
		listMove(this.moveId, -15);
	},
	'click #btnListDown': function(){									// Move down
		listMove(this.moveId, +15);
	}
});




// List C ///////////////////////////////////////////////////////////////////////////////////
// 					doubleSlug,								/main/group
// 					grp = _groupsGroup
// 					slg = ~
Session.set('currentBlogGroupId', 0);

Template.cmsListC.helpers({
	list: function() {
		Session.set('currentPostGroup', '_groupsGroup');
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var ret = Posts.find( {'page': p, 'language': l, 'groupSlug': '_groupsGroup'}, {sort: {timeStamp: 1}} );
		return ret;
	},
	listSelected: function() {
		return Session.equals('currentId', this._id) ? 'active' : '';
		// return Session.equals('currentBlogGroupId', this._id) ? 'active' : '';


	},
	deleteStatus: function() {
		var classActive = 'enabled';
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var query1 = Posts.find({'language': l, 'page': p, 'groupSlug': g, '_id': Session.get('currentId')} );
		var query2 = Posts.find({'language': l, 'page': p, 'groupSlug': g} );
		if (query2.count() < 2){ 														// If only one left, disable delete button
			classActive = 'disabled';
		}
		return {buttonId: Session.get('currentId'), classActive: classActive};
	},
	addDialogShow: function() {
		return Session.get('addDialogShow');
	},
	addDelPossible: function() { // in this page adding or deleting groups is not possible
		return ( getPageSlugBase() == 'inspiratie.om-te-delen' ) ? false: true;
	}
});

Template.cmsListC.events({
	'click #listAdd': function(){													// Add group
		Session.set('addDialogShow', true);
	},
	'click #listDelete': function(){											// Delete
		if (this.classActive == 'enabled') {
			var r = confirm("Let op:\nPagina's in deze groep worden ook verwijderd!\n\nDeze groep verwijderen?\n");
			if (r == true){
				Posts.remove( {_id: this.buttonId} );
			}
		}
	},
	'click .listBtn': function(){													// Select
		Session.set('currentId', this._id);
		Session.set('currentBlogGroupId', this._id);
	}
});
