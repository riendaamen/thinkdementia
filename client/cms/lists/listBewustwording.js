// List Bewustwording ///////////////////////////////////////////////////////////////////////////////////
// 					singleSlug, 				/main/item
// 					grp = _pagesGroup
// 					slg = ~

Template.cmsListBewustwording.helpers({
	list: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	listSelected: function() {
		return Session.equals('currentId', this._id) ? 'active' : '';
	}
});

Template.cmsListBewustwording.events({
	'click .listBtn': function(){											// Select in list
		Session.set('currentId', this._id);
	}
});
