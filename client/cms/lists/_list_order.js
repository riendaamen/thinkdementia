// Move up/down ////////////////////////////////////////////////////////////////////////

listMove = function(id, inc) {
	Posts.update( {'_id': id}, {$inc: {'order': inc}} );
	listMoveOrder();
};


listMoveOrder = function() {
	var l = Session.get('cmsLanguage');
	var p = getPageSlugBase();
	var g = getPostGroupSlug();

	if( getPageSlugBase() == 'agenda.agenda') { 	// execption for Agenda
		var list = Posts.find({'page': p, 'groupSlug': g, 'slug': { $ne:'main'} }, {sort: {order: 1}}).fetch();
	} else {
		var list = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': { $ne:'main'} }, {sort: {order: 1}}).fetch();
	}

	for ( var i=0; i < list.length; i++ ) { // sorry, this extra ordering is needed to fix list update
		var id = list[i]._id;									// no more than 10000 posts allowed
		var order = i - 10000;
		Posts.update( {'_id': id}, {$set: {'order': order}} );
	}
	
	for ( var i=0; i < list.length; i++ ) {
		var id = list[i]._id;
		var order = (i * 10) + 100;
		Posts.update( {'_id': id}, {$set: {'order': order}} );
	}

};
