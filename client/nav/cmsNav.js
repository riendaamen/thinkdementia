// Wait till Route is rendered, then show cmsNav
Template.cmsNav.helpers({
	routeReady: function() {
		var state = (Session.get('language') == -1) ? false : true;
		return state;
	}
});

// URL for frontpage
Template.cmsNavHead.helpers({
	link: function() {
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var name = getFrontPageName(x, y);
		return(Router.path(name));
	}
});



// Levels //////////////////////////////////////////////////////////

Template.cmsNavLevel1.helpers({
	buttons: function() {
		return getNavListLevel1();
	}	
});

Template.cmsNavLevel1.events({
	'click .btn': function(e) {								// Choose nav level 1
		e.preventDefault();
		Router.go( clickLevel1(this.index) );
	}
});



Template.cmsNavLevel2.helpers({
	show: function() {
		var show = (getNavListLevel2().length > 1) ? true : false;
		return show;
	},
	count: function() {
		var count = getNavListLevel2().length;
		return count;
	},
	buttons: function() {
		return getNavListLevel2();
	}
});

Template.cmsNavLevel2.events({
	'click .btn': function(e) {							// Choose nav level 2
		e.preventDefault();
		var x = Session.get('navLevel1');
		nav.level2Last[x] = this.index;
		Router.go( clickLevel2(this.index) );
	}
});



