
renderCufont2 = function() {
	Cufon.replace('.rd_navbar_title1', { fontFamily: 'MiloOT-Light'});
	Cufon.replace('.RD_topnav_subtitle', { fontFamily: 'MiloSerifOT-Medi'});
}



Template.headerlogo.helpers({
	text: function() {
		var l = Session.get('language');
		return (l==0) ? '3 Juni 2015 NatLab Eindhoven' : 'June 3rd 2015 NatLab Eindhoven';
	}
});
