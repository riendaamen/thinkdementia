// Wait till Route is rendered, then show frontNav /////////////

Template.frontNav.helpers({
	routeReady: function() {
		var state = (Session.get('language') == -1) ? false : true;
		return state;
	}
});



// simple navbar ////////////////////////////////////////////////////////////

Template.frontNavLevel1.helpers({
	buttons: function() {
		return getNavListLevel1();
	},
	classSelected: function() {
		// return Session.get('navLevel1');
		return (Session.get('navLevel1') == this.index) ? 'active' : '';
	}
});

Template.frontNavLevel1.events({
	'click .btn': function(e) {								// Choose nav level 1
		e.preventDefault();
		Router.go( clickLevel1(this.index) );
	}
});




Template.frontNavLevel2.helpers({
	count: function() {
		var count = getNavListLevel2().length;
		return count;
	},
	show: function() {
		return (getNavListLevel2().length > 1) ? true : false;
	},
	buttons: function() {
		return getNavListLevel2();
	}
});

Template.frontNavLevel2.events({
	'click .btn': function(e) {								// Choose nav level 2
		e.preventDefault();
		var x = Session.get('navLevel1');
		nav.level2Last[x] = this.index;
		Router.go( clickLevel2(this.index) );
	}
});
