Meteor.startup(function() {
	//clientLog('init');
	initNav();
});


// Nav //////////////////////////////////////////////////////

nav = {};

initNav = function() {

	Session.set('language', -1); 		// Start with empty nav

	Session.set('navLevel1', 0);
	Session.set('navLevel2', 0);

	Session.set('cmsLanguage', 0);
	Session.set('navCmsTab', 0)

	
	
	var array = [];									// level2Last 1D array
	for(var i = 0; i < pages.length; i++) {
		array.push(0);
	}
	nav.level2Last = array;

	
	
	var array = new Array();				// last cmsTab, 2D array
	for(var i = 0; i < pages.length; i++) {
		var array2 = new Array();
		for(var j = 0; j < pages[i].level2.length; j++) {
			array2.push(0);
		}
		array.push(array2);
	}
	nav.cmsLast = array;
	nav.cmsMode = false;
	
	
	
	var array = new Array();				// last editTab, 3D array
	for(var i = 0; i < pages.length; i++) {
		var array2 = new Array();
		for(var j = 0; j < pages[i].level2.length; j++) {
			var array3 = new Array();
			for(var k = 0; k < 8; k++) {			// max 8 editTabs
				array3.push(0);
			}
			array2.push(array3);
		}
		array.push(array2);
	}
	nav.editLast = array;

	
}



