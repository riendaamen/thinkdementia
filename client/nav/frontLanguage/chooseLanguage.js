

// choose front language B ////////////////////////////////////////////////////////////////////////////////

Template.chooseLanguage2.helpers({

	buttonsLang: function() {
		var list = [];
		var lang = Session.get('language')
		var i = languages[lang];
		var item = {};
		item.buttonNameLang = i.nameLong;
		item.index = lang;
		item.classSelected = 'temp-selected';
		list.push(item);
		return list;
	}
});

Template.chooseLanguage2.events({
	'click .buttonLang': function(e) {						// Choose Language
		e.preventDefault();
		var i = 1-this.index;
		Session.set('language', i)
		var level1 = Session.get('navLevel1');
		var level2 = Session.get('navLevel2');
		clientLog('choose lang = ' + i);
		Router.go(getPageName(level1, level2, nav.CmsMode));

	},
	'click .btn1': function(e) {									// Choose nav level 1
		e.preventDefault();
		Router.go( clickLevel1(this.index) );
	},
	'click .btn2': function(e) {									// Choose nav level 2
		e.preventDefault();
		var x = Session.get('navLevel1');
		nav.level2Last[x] = this.index;
		clientLog('choose level1 = ' + x + ' level2 = ' + this.index);
		Router.go( clickLevel2(this.index) );
	}
});
