
// get current route

UI.registerHelper('currentURL', function() {
	return  Router.current().url;
});


// Goto page in navbars, used by front and cms ////////////////////////////

clickLevel1 = function(index) {
	var y = nav.level2Last[index];		// remember last visited level 2
	return(getPageName(index, y, nav.cmsMode));
}

clickLevel2 = function(index) {
	var x = Session.get('navLevel1');
	return(getPageName(x, index, nav.cmsMode));
}



// Nav buttons /////////////////////////////////////////////////////

getNavListLevel1 = function() {
	var list = [];
	var language = Session.get('language');
	if (nav.cmsMode == true) language = 0;

	for(var i = 0; i < pages.length; i++) {
		var item = {};
		var sel = (i == Session.get('navLevel1')) ? 'active' : '';
		item.classSelected = 	sel;
		item.index = 					i;
		item.buttonName = 		pages[i].btnName[language];
		item.myUrl = 					clickLevel1(i);
		list.push(item);
	}
	return list;
};



getNavListLevel1Front = function() {
	var list = [];
	var language = Session.get('language');
	if (nav.cmsMode == true) language = 0;

	for(var i = 6; i < (pages.length); i++) {
		var item = {};
		var sel = (i == Session.get('navLevel1')) ? 'active' : '';
		item.classSelected = 	sel;
		item.index = 					i;
		item.buttonName = 		pages[i].btnName[language];
		item.myUrl = 					clickLevel1(i);
		list.push(item);
	}
	return list;
};


getNavListLevel1FrontI = function(i) {
	var list = [];
	var language = Session.get('language');
	if (nav.cmsMode == true) language = 0;

		var item = {};
		var sel = (i == Session.get('navLevel1')) ? 'active' : '';
		item.classSelected = 	sel;
		item.index = 					i;
		item.buttonName = 		pages[i].btnName[language];
		item.myUrl = 					clickLevel1(i);
		list.push(item);

	return list;
};



getNavListLevel2 = function() {
	var list = [];
	var language = Session.get('language');
	if (nav.cmsMode == true) language = 0;
	var pageLevel1Index = Session.get('navLevel1');
	if (pageLevel1Index < 9) {			//skip level 2 buttons for level1 == extra
		var level2 = pages[pageLevel1Index].level2;
		for(var i = 0; i < level2.length; i++) {
			var item = {};
			var sel = (i == Session.get('navLevel2')) ? 'active' : '';
			item.classSelected = 	sel;
			item.index = 					i;
			item.buttonName = 		pages[pageLevel1Index].level2[i].btnName[language];
			item.myUrl = 					clickLevel2(i);
			list.push(item);
		}
	}
	return list;
};
