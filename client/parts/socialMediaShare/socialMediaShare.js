// Social media agenda //////////////////////////////////////////////////////////////////////////


Template.front_blog_social_media.rendered = function() {
	addthis.toolbox(".addthis_toolbox");
	addthis_config = {"data_track_addressbar": false};
};



Template.front_blog_social_media.helpers({
	socMed: function() {
		var title = this.title;
		var descr = this.shortDescr;
		var url = Router.current().url;
		$('meta[property="og:title"]').replaceWith('<meta property="og:title" content="' +  title + '">');
		$('meta[property="og:description"]').replaceWith('<meta property="og:description" content="' + descr + '">');

		$('meta[property="og:url"]').replaceWith('<meta property="og:url" content="' + url + '">');
		return ('');
	},
	AddthisUrl: function() {
		var link;
		link = Router.current().url;
		return (link);
	},
	AddthisTitle: function(){
		var str = this.title;
		return str;
	},
	AddthisDescription: function(){
		var str = this.shortDescr;
		return str;
	}
});








// social media sensetalk ///////////////////////////////////////////////////////////////////////

Template.front_blog_social_media_sensetalk.rendered = function() {
	addthis.toolbox(".addthis_toolbox");
	addthis_config = {"data_track_addressbar": false};

};

Template.front_blog_social_media_sensetalk.helpers({
	socMed: function() {
		var title = this.title;
		var url = Router.current().url;
		clientLog('setting meta');
		$('meta[property="og:title"]').replaceWith('<meta property="og:title" content="' +  title + '">');
		$('meta[property="og:url"]').replaceWith('<meta property="og:url" content="' + url + '">');
		return ('');
	},
	AddthisUrl: function() {
		var link;
		link = Router.current().url;
		return (link);
	},
	AddthisTitle: function(){
		var str = this.title;
		return str;
	},
	AddthisDescription: function(){
		var str = this.colF_text;
		return str;
	}
});
