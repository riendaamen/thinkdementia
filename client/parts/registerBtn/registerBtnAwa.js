// registerBtn Awa ////////////////////////////////////////////////////////

Template.registerBewustwBtn.helpers({
	regOpen: function() {

		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = Session.get('navParam1');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s} ).fetch()[0];

		// clientLog('regConferenceOpen = ' + query.openReg);
		var show = (query.openReg == 'checked') ? true : false;
		return show;
	},




	showForm: function() {
		return Session.get('showFormAwa');
	},
	showFormThanks: function() {
		return Session.get('showFormAwaThanks');
	}
});


Template.registerBewustwBtn.events({
	'click #registerBtn': function(e){												// register
		e.preventDefault();
		// clientLog ('register for symposium');
		$('body,html').scrollTop(0);
		Session.set('showFormAwa', true);
	}
});
