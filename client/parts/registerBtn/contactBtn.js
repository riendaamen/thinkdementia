// contact btn ////////////////////////////////////////////////////////

Template.contactBtn.helpers({
	showForm: function() {
		return Session.get('showFormContact');
	},
	showFormThanks: function() {
		return Session.get('showFormContactThanks');
	}
});


Template.contactBtn.events({
	'click #formContactBtn': function(e){												// contact
		e.preventDefault();
		$('body,html').scrollTop(0);
		Session.set('showFormContact', true);
	}
});


