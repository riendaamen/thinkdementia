
// registerBtn symposium ////////////////////////////////////////////////////////

Template.registerBtn.helpers({
	regConferenceOpen: function() {
		var p = 'programma.conference';
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': 0, 'page': p, 'groupSlug': g, 'slug': s}).fetch()[0];
		// clientLog('regConferenceOpen = ' + query.openReg);
		var show = (query.openReg == 'checked') ? true : false;
		return show;
	},
	showForm: function() {
		return Session.get('showFormSymposium');
	},
	showFormThanks: function() {
		return Session.get('showFormThanks');
	}
});


Template.registerBtn.events({
	'click #registerBtn': function(e){												// register
		e.preventDefault();
		// clientLog ('register for symposium');
		$('body,html').scrollTop(0);
		Session.set('showFormSymposium', true);
	}
});
