// Social media agenda //////////////////////////////////////////////////////////////////////////

Template.front_blog_social_media.rendered = function() {
	addthis.toolbox(".addthis_toolbox");
	addthis_config = {"data_track_addressbar": false};
};


Template.front_blog_social_media.helpers({
	socMed: function() {
		var title = this.title;
		var descr = this.shortDescr;
		var url = Router.current().url;
		$('meta[property="og:title"]').replaceWith('<meta property="og:title" content="' +  title + '">');
		$('meta[property="og:description"]').replaceWith('<meta property="og:description" content="' + descr + '">');
		$('meta[property="og:url"]').replaceWith('<meta property="og:url" content="' + url + '">');
		return ('');
	}
});




// social media sensetalk ///////////////////////////////////////////////////////////////////////

Template.front_blog_social_media_sensetalk.rendered = function() {
	addthis.toolbox(".addthis_toolbox");
	addthis_config = {"data_track_addressbar": false};
};


Template.front_blog_social_media_sensetalk.helpers({
	socMed: function() {
		var title = this.title;
		var url = Router.current().url;
		clientLog('setting meta');
		$('meta[property="og:title"]').replaceWith('<meta property="og:title" content="' +  title + '">');
		$('meta[property="og:url"]').replaceWith('<meta property="og:url" content="' + url + '">');
		return ('');
	}

});
