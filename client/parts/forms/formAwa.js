// form awa	//////////////////////////////////////////

Template.formAwa.helpers({
	text_formSympZend: function() {
		var l = Session.get('language');
		// return (l==0) ? 'Verzenden' : 'Send';
		return (l==0) ? 'Registreer' : 'Register';
	}
});


Template.formAwa.events({
	'click .btnPopupClose': function(){								// Close
		Session.set('showFormAwa', false);
	},
	'click #formSympSubmit': function(e){							// Submit
		e.preventDefault();
		var valid = true;

		if ( (!isValidEmailAddress($('#email').val())) && ($('#telefoon').val()=='') )   {
			valid = false;
			showWarning("#email");
			showWarning("#telefoon");
		}
			
		
		if(valid) {																		// Insert registration
			// clientLog( 'naam ' + $('#naam').val() );
			// clientLog( 'emai ' + $('#email').val() );
			// clientLog( 'tele ' + $('#telefoon').val() );
			Session.set('showFormAwa', false);
			Session.set('showFormAwaThanks', true);
			
			var id = Registrations.insert({
				timeStamp: 			(new Date()).getTime(),
				timeTextDay:		(new Date()).toString().substr(4,6),
				timeTextTime:		(new Date()).toString().substr(16,5),
				track:					'awareness',		// conference|awareness
				
				sessionId:			this.id,
				sessionName:		this.slug,
				
				naam: 					$('#naam').val(),
				email: 					$('#email').val(),
				telefoon: 			$('#telefoon').val(),
			});
		}
	}	
});




Template.text_formAwaText.helpers({
	text_formSympZend: function() {
		var l = Session.get('language');
		// return (l==0) ? 'Verzenden' : 'Send';
		return (l==0) ? 'Registreer' : 'Register';
	}
});
