// form contact	///////////////////////////////////////////////////////////////////////////////////

Template.formContact.helpers({
	text_formContactZend: function() {
		var l = Session.get('language');
		return (l==0) ? 'Verzenden' : 'Send';
	},
	text_formContactOpm: function() {
		var l = Session.get('language');
		return (l==0) ? 'Vul hier uw opmerkingen/vragen in.' : 'Fill in your comments/questions.';
	}
});

Template.formContact.events({
	'click .btnPopupClose': function(){										// Select group
		Session.set('showFormContact', false);
	},
	'click #submitContactForm': function(e){							// Submit
		e.preventDefault();
		clientLog( 'contact btn'  );
		var valid = true;

		if ( (!isValidEmailAddress($('#email').val())) && ($('#telefoon').val()=='') )   {
			valid = false;
			showWarning("#email");
			showWarning("#telefoon");
		}

		if (/\S/.test($('#opmerkingen').val())) {
		}
		else{
			valid = false;
			showWarning("#opmerkingen");

		}


		if(valid) {																		// Send email

			var m = '';
			var c = "<tr><td style='color:#C7B296; width: 40%;'>"

			m = m + "<table border='0' style='width:520px; color: #53473F; font-size: 12px; border-spacing: 2px; padding: 14px; margin: 10px; line-height: 20px; font-family:verdana; border: 0px;'>";
			m = m + "<caption style='font-weight: bold; text-align: left; margin-left: 18px;'>Contact formulier</caption>";

			m = m + c + 'Naam: '				+ '</td></td>' + $('#naam').val() 							+ '</td></tr>';
			m = m + c + 'Email: '				+ '</td></td>' + $('#email').val() 							+ '</td></tr>';
			m = m + c + 'Telefoon: '		+ '</td></td>' + $('#telefoon').val() 					+ '</td></tr>';
			m = m + c + 'Opmerkingen: '	+ '</td></td>' + $('#opmerkingen').val() 				+ '</td></tr>';

			m = m + "</table>"

			var email = $('#email').val();

			clientLog('send email: ' + m);
			Meteor.call('emailContactUs', email, 'Think Dementia, contact formulier', m);

			Session.set('showFormContact', false);
			Session.set('showFormContactThanks', true);
			
		}
	}
});
