// form Conf thanks	///////////////////////////////////////////////////////////////////////////////////

Template.formThanks.helpers({
	text_formThanksZend: function() {
		var l = Session.get('language');
		// return (l==0) ? 'Prima!' : 'Fine!';
		return (l==0) ? 'Okay' : 'Okay';
	}
});

Template.formThanks.events({
	'click .btnPopupClose': function(){									// close
		Session.set('showFormSymposium', false);
		Session.set('showFormThanks', false);
	},
	'click #formSympSubmit': function(e){								// submit
		e.preventDefault();
		Session.set('showFormSymposium', false);
		Session.set('showFormThanks', false);
	}	
});




// form Awa thanks	///////////////////////////////////////////////////////////////////////////////////

Template.formAwaThanks.helpers({
	text_formThanksZend: function() {
		var l = Session.get('language');
		return (l==0) ? 'Okay' : 'Okay';
	}
});

Template.formAwaThanks.events({
	'click .btnPopupClose': function(){									// close
		Session.set('showFormAwa', false);
		Session.set('showFormAwaThanks', false);
	},
	'click #formSympSubmit': function(e){								// submit
		e.preventDefault();
		Session.set('showFormAwa', false);
		Session.set('showFormAwaThanks', false);
	}	
});




// form Contact thanks	///////////////////////////////////////////////////////////////////////////////////

Template.formContactThanks.helpers({
	text_formThanksZend: function() {
		var l = Session.get('language');
		return (l==0) ? 'Okay' : 'Okay';
	}
});

Template.formContactThanks.events({
	'click .btnPopupClose': function(){									// close
		Session.set('showFormContact', false);
		Session.set('showFormContactThanks', false);
	},
	'click #formSympSubmit': function(e){								// submit
		e.preventDefault();
		Session.set('showFormContact', false);
		Session.set('showFormContactThanks', false);
	}	
});


