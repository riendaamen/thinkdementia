// form symposium	///////////////////////////////////////////////////////////////////////////////////

Template.formSymposium.helpers({
	text_formSympZend: function() {
		var l = Session.get('language');
		// return (l==0) ? 'Verzenden' : 'Send';
		return (l==0) ? 'Registreer' : 'Register';
	}
});


Template.formSymposium.events({
	'click .btnPopupClose': function(){								// Close
		Session.set('showFormSymposium', false);
	},


	'click #test': function(){												// Test
		clientLog('test');
		var radios = document.getElementsByName('trackVoorkeur');
		for (var i = 0, length = radios.length; i < length; i++) {
				if (radios[i].checked) {
						clientLog(radios[i].value);
						break;
				}
		}
	},





	'click #formSympSubmit': function(e){							// Submit
		e.preventDefault();
		var valid = true;

		if ($('#naam').val() == '') {
			valid = false;
			showWarning("#naam");
		}

		// if ($('#email').val() == '') {
		if ( !isValidEmailAddress($('#email').val()) ) {
			valid = false;
			showWarning("#email");
		}

		if ($('#bedrijf').val() == '') {
			valid = false;
			showWarning("#bedrijf");
		}

		if ($('#nationaliteit').val() == '') {
			valid = false;
			showWarning("#nationaliteit");
		}

		if(valid) {																		// Insert registration

			var voorkeur;
			var radios = document.getElementsByName('trackVoorkeur');
			for (var i = 0, length = radios.length; i < length; i++) {
					if (radios[i].checked) {
							voorkeur = radios[i].value;
							break;
					}
			}


			var id = Registrations.insert({
				timeStamp: 			(new Date()).getTime(),
				timeTextDay:		(new Date()).toString().substr(4,6),
				timeTextTime:		(new Date()).toString().substr(16,5),
				track:					'conference',		// conference|awareness
				trackVoorkeur:	voorkeur,
				naam: 					$('#naam').val(),
				email: 					$('#email').val(),
				telefoon: 			$('#telefoon').val(),
				bedrijf: 				$('#bedrijf').val(),
				functie: 				$('#functie').val(),
				nationaliteit: 	$('.crs-country').val()
			});

			Session.set('showFormSymposium', false);
			Session.set('showFormThanks', true);


		}
	}

});
