
renderCufont = function() {
	Cufon.replace('.imgwrapper .content, .titlelook, .listbox-inside a.siteurl, .listbox-inside h5', { fontFamily: 'MiloSerifOT-Medi' });
	Cufon.replace('.cufstyle', { fontFamily: 'MiloSerifOT-Medi',	hover: { color: 'black'	}});
	Cufon.replace('.agenda li a, .news li a', { fontFamily: 'MiloSerifOT-Medi',	hover: { color: 'black'	}});
	Cufon.replace('#menu2 > ul > li', { fontFamily: 'MiloOT-Light' , hover : true, hoverables: { li: true }, ignore: { ul: true } });
	Cufon.replace('#menu2 li.main ul li a', { fontFamily: 'MiloOT-Light',	hover: { color: 'white'	} });
	Cufon.replace('#menu2 li a.left-active, #menu li a.active, #menu li a.right-active', { fontFamily: 'MiloOT-Light'});
	Cufon.replace('h1, h2, .datebox p, .datebox p span, a.send', { fontFamily: 'MiloOT-Light'});

}
