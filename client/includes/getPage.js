//
// Get pageIndex, used for Collection ID
//
// unique page-index based on current
// 	level1, level2, language, param1 and param2
//
//	results:	name1.name2(.x.y)
//						en.name1.name2(.x.y)
//
getPageIndex = function() {
	var iLang = Session.get('language');
	if (nav.cmsMode == true) iLang = Session.get('cmsLanguage');

	var nameLang = (iLang == 0) ? '' : languages[iLang].path + '.';
	var name1 = (pages[Session.get('navLevel1')].urlName[iLang]);
	var name2 = (pages[Session.get('navLevel1')].level2[Session.get('navLevel2')].urlName[iLang]);
	var p1 = (nav.param1 == '') ? '' : '.' + nav.param1;
	var p2 = (nav.param2 == '') ? '' : '.' + nav.param2;

	var pageIndex = nameLang + name1 + '.' + name2 + p1 + p2;

	return pageIndex;
}




//
// Get page name, used for navigation
//
//	input: level1, level 2, cmsMode
//
// 	level1, level2						front + cms 		Route.go
//	set language in front			front						Route.go
//	X (view website in cms		front						place URL in <a>,	uses cmsMode = false)
//
// 	name1.name2
// 	en.name1.name2
//	cms.name1.name2
//
getPageName = function(level1, level2, cmsMode) {
	var iLang = Session.get('language');
	var nameLang = (iLang == 0) ? '' : ( languages[iLang].path + '.' );

	if (cmsMode == true) iLang = 0;
	var nameCms = (cmsMode == true) ? 'cms.' :	'';

	var name1 = (pages[level1].urlName[iLang]);
	var name2 = (pages[level1].level2[level2].urlName[iLang]);
	var name = nameCms + nameLang + name1 + '.' + name2;

	// clientLog('pageName = ' + name);
	return(name);
}




//
// Get slug base URL
//	used by 	singleSlug and doubleSlug
//						post in blog
//
getPageSlugBase = function() {
	var level1 = Session.get('navLevel1');
	var level2 = Session.get('navLevel2');

	var name1 = (pages[level1].urlName[0]);
	var name2 = (pages[level1].level2[level2].urlName[0]);
	var name = name1 + '.' + name2;
	//clientLog('PageSlugBase = ' + name);
	return(name);
}

//
// Get slug base URL				// dual language
//	used by 	singleSlug and doubleSlug
//						post in blog
//
getPageSlugBaseDualLang = function() {
	var lang = Session.get('language');
	var level1 = Session.get('navLevel1');
	var level2 = Session.get('navLevel2');

	var name1 = (pages[level1].urlName[lang]);
	var name2 = (pages[level1].level2[level2].urlName[lang]);
	var name = name1 + '.' + name2;
	clientLog('PageSlugBase = ' + name);
	return(name);
}










//
// View website in cms				front						place URL in <a>,	uses cmsMode = false
//
getFrontPageName = function(level1, level2) {
	var iLang = Session.get('cmsLanguage');
	var nameLang = (iLang == 0) ? '' : ( languages[iLang].path + '.' );
	var name1 = (pages[level1].urlName[iLang]);
	var name2 = (pages[level1].level2[level2].urlName[iLang]);
	var name = nameLang + name1 + '.' + name2;
	// clientLog('frontPageName = ' + name);
	return(name);
}
