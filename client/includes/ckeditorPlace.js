//
// Place CKEditor
//	this file is not allowed inder the name ckEditor.js
//
placeCKeditor = function(inputName){

  CKEDITOR.replace(inputName, {
    filebrowserBrowseUrl: 		  '/upload_file',
    filebrowserImageBrowseUrl: 	'/upload_image',
    filebrowserWindowWidth:     '640',
    filebrowserWindowHeight:    '520',
		contents:										{extraPlugins: 'onchange'},
    resize_enabled :            true
  });
	
  CKEDITOR.on('dialogDefinition', function(ev) {
		// Take the dialog name and its definition from the event data.
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;

		// Check if the definition is from the dialog we're
		// interested on (the Link dialog).
		if (dialogName == 'link') {
			dialogDefinition.removeContents('advanced');
			// Get a reference to the "Target" tab.
			var targetTab = dialogDefinition.getContents('target');
			// Set the default value for the URL field.
			var targetField = targetTab.get('linkTargetType');
			targetField['default'] = '_blank';
		}

		if (dialogName == 'image') {
			dialogDefinition.removeContents('advanced');
			dialogDefinition.removeContents('Link');
		}
	});

	CKEDITOR.on('instanceReady', function (e) {
    e.editor.on('change', function (ev) {
			Session.set('edited', true);
    });
	});

}
