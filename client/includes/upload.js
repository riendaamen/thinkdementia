// CMS image upload ////////////////////////////////////////////////////////////////////

Session.set('myCKEditorFuncNum', 0);	// get functnum from CKeditor
Session.set('myImgFileId', '');				// file ID


Template.upload_image.helpers({
	funcNum: function() {
		return Session.get('myCKEditorFuncNum');
	},
	imageList: function() {				// show image
		var myId = Session.get('myImgFileId');
		var ret = DB_pix_article.find({_id: myId}, {});
		return ret;
	}
});


Template.upload_image.events({
	'click #sendUrl': function(e,t){													// OK, send url of picture to CKeditor
		var fileUrl = this.url();
		var funcNum = Session.get('myCKEditorFuncNum');
		window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
		window.close();
	},
	'click #cancelUrl': function(e,t){												// Cancel
		this.remove();
		window.close();
	},
	'change .fileUploader': function (event, template) { 			// Upload file
		var file_id;
		FS.Utility.eachFile(event, function(file) {
			DB_pix_article.insert(file, function (err, fileObj) {	// inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
				file_id = fileObj._id;
				Session.set( 'myImgFileId', String(file_id) );
			});
		});
	}
});



// CMS file upload ////////////////////////////////////////////////////////////////////

Template.upload_file.helpers({
	// funcNum: function() {
		// return Session.get('myCKEditorFuncNum');
	// },
	imageList: function() {				// just the first one
		var myId = Session.get('myImgFileId');
		var ret = DB_file_article.find({_id: myId}, {});
		return ret;
	}
});

Template.upload_file.events({
	'click #sendUrl': function(e,t){														// OK, send url of picture to CKeditor
		var fileUrl = this.url();
		var funcNum = Session.get('myCKEditorFuncNum');
		window.opener.CKEDITOR.tools.callFunction( funcNum, fileUrl );
		window.close();
	},
	'click #cancelUrl': function(e,t){													// Cancel
		this.remove();
		window.close();
	},
	'change .fileUploader': function (event, template) { 				// Upload file
		var file_id;
		FS.Utility.eachFile(event, function(file) {
			DB_file_article.insert(file, function (err, fileObj) {	// inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
				file_id = fileObj._id;
				Session.set( 'myImgFileId', String(file_id) );
			});
		});
	}
});
