//
// Here we can generate groups in the Blogs per page
//	this is needed after an Meteor reset and fills in a few needed standard groups
//
//
// this can optionaly also be done by the user in the cms with -add-
//
//


initFill_sharing = function () {
	clientLog('init fill in website with groups');

	//
	// Add groups
	//
	// addPost = function(slug, title, language)

	Session.set('currentPostGroup', '_groupsGroup');

	Session.set('navLevel1', 4);
	Session.set('navLevel2', 2);

	addPost('muziek', 'Muziek', 0);
	addPost('boeken', 'Boeken', 0);
	addPost('filmpjes', 'Filmpjes', 0);
	addPost('cinemaNL', 'Cinema', 0);

	addPost('music', 'Music', 1);
	addPost('books', 'Books', 1);
	addPost('short-films', 'Short films', 1);
	addPost('cinemaEN', 'Cinema', 1);

};


initFill_anderenoverons = function () {
	clientLog('init fill in website with groups');

	//
	// Add groups
	//
	// addPost = function(slug, title, language)

	Session.set('currentPostGroup', '_groupsGroup');

	Session.set('navLevel1', 5);
	Session.set('navLevel2', 5);

	addPost('algemene-ervaringen', 'Algemene ervaringen', 0);
	addPost('coaching', 'Coaching', 0);
	addPost('oneness-awakening-cursus', 'Oneness awakening cursus', 0);
	addPost('onederfull-awereness-cursus', 'Onederfull awereness cursus', 0);
	addPost('workshop-onederfull-purpose', 'Workshop onederfull purpose', 0);
	addPost('workshop-overvloed-en-rijkdom', 'Workshop overvloed en rijkdom', 0);
	addPost('workshop-liefdevolle-en-duurzame-relaties', 'Workshop liefdevolle en duurzame relaties', 0);
	addPost('workshop-genezing-en-gezondheid', 'Workshop genezing en gezondheid', 0);
	addPost('meditatie', 'meditatie', 0);

	addPost('experiences', 'Experiences', 1);
	addPost('coaching', 'Coaching', 1);
	addPost('oneness-awakening-course', 'Oneness awakening course', 1);
	addPost('onederful-awareness-course', 'Onederful awareness course', 1);
	addPost('workshop-onederful-purpose', 'Workshop onederful purpose', 1);
	addPost('workshop-abundance-endwealth', 'Workshop abundance endwealth', 1);
	addPost('workshop-loving-and-lasting-realtionships', 'Workshop loving and lasting realtionships', 1);
	addPost('workshop-healing-and-health', 'Workshop healing and health', 1);
	addPost('workshop-meditation', 'Workshop meditation', 1);
	
};
