// Think Dementia

// Create routes from pages, must be first because of index in array Router.routes[]

index = 0;
// console.log('Init Routes.');
for(var i1 = 0; i1 < pages.length; i1++) {																		// level 1
	for(var i2 = 0; i2 < pages[i1].level2.length; i2++) {												// Level 2
		for(var iCms = 0; iCms < 2; iCms++) {																			// Front / cms
			for(var iLang = 0; iLang < languages.length; iLang++) {									// Language
				if(iCms == 0 || iLang < 1) {

					var nameCms = (iCms == 1) ? 'cms.' :	'';
					var nameLang = (iLang == 0) ? '' : ( languages[iLang].path + '.' );
					var name1 = (pages[i1].urlName[iLang]);
					var name2 = (pages[i1].level2[i2].urlName[iLang]);
					var name = nameCms + nameLang + name1 + '.' + name2;

					var urlCms = (iCms == 1) ? 'cms/' :	'';
					var urlLang = (iLang == 0) ? '' : ( languages[iLang].path + '/' );
					var url1 = (pages[i1].urlName[iLang]);
					var url2 = ((i2 == 0) && (i1 !== 1)) ? '' : ( '/' + (pages[i1].level2[i2].urlName[iLang]) );
					var url = urlCms + urlLang + url1 + url2;


					// Add routes for languages and cms ////////////////////////////////////////////
					Router.route(name, {
						path: url,
						action: function(){
							var level1 = this.route.level1
							var level2 = this.route.level2
							var lastCmsTab = nav.cmsLast[level1][level2];
							Session.set('navEditTab', nav.editLast[level1][level2][lastCmsTab]);
							Session.set('navCmsTab', lastCmsTab); // Needs to be first; prevents old cms tabs from existing in new url
							Session.set('navLevel1', level1);
							Session.set('navLevel2', level2);
							Session.set('language', this.route.language);
							nav.cmsMode = this.route.cmsMode;
							nav.param1 = '';
							nav.param2 = '';
							// console.log('routerName = ' + this.route.path());
							this.render();
						}
					});

					Router.routes[index].level1 = i1;
					Router.routes[index].level2 = i2;
					Router.routes[index].language = iLang;
					var rCms = (iCms == 1) ? true : false;
					Router.routes[index].cmsMode = rCms;
					var rTemplate;
					rTemplate = pages[i1].level2[i2].template;
					if (iCms == 1) rTemplate = rTemplate + 'Cms';
					Router.routes[index].options.template = rTemplate;
					var rLayout = (iCms == 1) ? 'cmsLayout' : 'frontLayout';
					Router.routes[index].options.layoutTemplate = rLayout;
					index++;



					// Add routes for slugs /////////////////////////////////////////////////////////

					// if (iCms == 0 && iLang == 0) {		// Only for Front and first language
					if (iCms == 0) {									// Front poly language

						var pathType = pages[i1].level2[i2].pathType;
						if (pathType == 'singleSlug' || pathType == 'doubleSlug') {	// singleSlug
							// console.log('Router added for singleSlug');
							Router.route(name + '_slug1', {
								path: url + '/:x',
								action: function(){
									var level1 = this.route.level1
									var level2 = this.route.level2
									Session.set('navCmsTab', 0);
									Session.set('navLevel1', level1);
									Session.set('navLevel2', level2);
									Session.set('language', this.route.language);
									checkLanguageSlug();
									nav.cmsMode = false;
									nav.param1 = this.params.x;
									nav.param2 = '';
									Session.set('navParam1', this.params.x);
									this.render();
								}
							});
							Router.routes[index].level1 = i1;
							Router.routes[index].level2 = i2;
							Router.routes[index].language = iLang;
							Router.routes[index].options.template = rTemplate + '_slug1';
							Router.routes[index].options.layoutTemplate = rLayout;
							index++;
						}

						if (pathType == 'doubleSlug'){												// doubleSlug
							//console.log('Router added for doubleSlug');
							Router.route(name + '_slug2', {
								path: url + '/:x/:y',
								action: function(){
									var level1 = this.route.level1
									var level2 = this.route.level2
									Session.set('navCmsTab', 0);
									Session.set('navLevel1', level1);
									Session.set('navLevel2', level2);
									Session.set('language', this.route.language);
									checkLanguageSlug();
									nav.cmsMode = false;
									nav.param1 = this.params.x;
									nav.param2 = this.params.y;
									Session.set('navParam1', this.params.x);
									Session.set('navParam2', this.params.y);
									this.render();
								}
							});
							Router.routes[index].level1 = i1;
							Router.routes[index].level2 = i2;
							Router.routes[index].language = iLang;
							Router.routes[index].options.template = rTemplate + '_slug2';
							Router.routes[index].options.layoutTemplate = rLayout;
							index++;
						}
					}
				}
			}
		}
	}
}



Router.configure({
	//layoutTemplate: 'layout',
	loadingTemplate: 'loading',
	notFoundTemplate: 'notFound',
	waitOn: function() {
		Meteor.subscribe('images');
		Meteor.subscribe('imagesCarousel');
		Meteor.subscribe('imagesSlide');
		Meteor.subscribe('imagesThumb');
		Meteor.subscribe('imagesPhoto');
		Meteor.subscribe('imagesCustomer');
		Meteor.subscribe('coll_pix_article');
		Meteor.subscribe('coll_file_article');
		Meteor.subscribe('registrations');

		// Excel export
		//Meteor.subscribe('temporaryFiles');



		return Meteor.subscribe('posts');		// Wait till subscribed on collections
	}
});


// Additional routes ///////////////////////////////////////////////////////////////

// Home
Router.route('home', {
	path: '/',
	// path: '/xyz',		// temp hide home pageX
	template: pages[0].level2[0].template,
	layoutTemplate: 'frontLayout',
	action: function(){
		Session.set('navLevel1', 0);
		Session.set('navLevel2', 0);
		Session.set('language', 0);
		nav.cmsMode = false;
		nav.param1 = '';
		nav.param2 = '';
		Session.set('navCmsTab', 0);
		this.render();
	}
});

// CMS
Router.route('cms', {
	path: 'cms',
	template: pages[0].level2[0].template + 'Cms',
	layoutTemplate: 'cmsLayout',
	action: function(){
		Session.set('navLevel1', 0);
		Session.set('navLevel2', 0);
		Session.set('language', 0);
		nav.cmsMode = true;
		nav.param1 = '';
		nav.param2 = '';
		Session.set('navCmsTab', 0);
		this.render();
	}
});


// upload image
Router.route('upload_image', {
	path: '/upload_image',
	template: 'upload_image',
	action: function(){
		Session.set('myCKEditorFuncNum', this.params.query.CKEditorFuncNum );
		this.render();
	}
});

// upload file
Router.route('upload_file', {
	path: '/upload_file',
	template: 'upload_file',
	action: function(){
		Session.set('myCKEditorFuncNum', this.params.query.CKEditorFuncNum );
		this.render();
	}
});


// Loading Think Dementia //////////////////////////////////////////////////

// Router.onBeforeAction('loading');
Router.onBeforeAction(function(pause) {
  if (!this.ready()) {
    this.render('loading');
    pause(); 		// otherwise the action will just render the main template.
  }
	$('body,html').scrollTop(0); 	// make sure to scroll to the top of the page on a new route
	// console.log('before');
	this.next();
});

// Router.onAfterAction(function(pause) {
	// renderCufont();
	// console.log('after');
// });
