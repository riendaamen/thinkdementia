// set SEO from post ////////////////////////////////////////////////////////

getSEO_fromPost = function(post) {
	var p = post.fetch()[0];
	document.title = p.seoTitle;
	document.getElementById("description").setAttribute("content", p.seoDescr);
}


//
// Place SEO in <head> and meta description
//
placeSEO = function(seoTitle, seoDescr) {
	clientLog('placeSEO 1, title = ' + seoTitle);
	clientLog('placeSEO 2, descr = ' + seoDescr);
	document.title = seoTitle;
}



//
// Set SEO for slug
//
setSEOSlug = function() {

	// clientLog('Set Seo for slug');

	var seoTitle = '...';
	var seoDescr = '...';

	var page = getPageSlugBase();
	var group = Session.get('currentBlogGroupId');
	var slugx = Session.get('currentId');

	var id = Posts.find({'page': page, 'groupSlug': group, 'slug': slugx});
	seoTitle = id.fetch()[0].seoTitle;
	seoDescr = '...slug';
	placeSEO(seoTitle, seoDescr);
}



//
// Set SEO for group
//
setSEOGroup = function() {
	clientLog('Set Seo for group');
	var page = getPageSlugBase();
	var id = BlogGroups.find({'page': page, 'groupSlug': Session.get('currentBlogGroupId')});
	seoTitle = id.fetch()[0].seoTitle;
	seoDescr = '...';
	placeSEO(seoTitle, seoDescr);

}
