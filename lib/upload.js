
// Image upload

DB_pix_article = new FS.Collection("coll_pix_article", {
	stores: [
		new FS.Store.FileSystem("coll_pix_article_01", {
			transformWrite: function(fileObj, readStream, writeStream) {	// Transform the image, max px size
				console.log('upload image for article');
				var w2 = 877;	// output
				var h2 = 877;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {		// Equal size
							this.stream().pipe(writeStream);
						} else if (r1<r2) {					// Portrait
							if ( h1>h2 )	{
								this.resize(null, h2).stream().pipe(writeStream);
							} else {
								this.stream().pipe(writeStream);
							}
						} else {							// Landscape
							if (w1>w2){
								this.resize(w2).stream().pipe(writeStream);
							} else {
								this.stream().pipe(writeStream);
							}
						}
				  }
				  else{
						console.log('RD: Error on fileUpload for coll_images.');
				  }
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});


DB_file_article = new FS.Collection("coll_file_article", {
	stores: [
		new FS.Store.FileSystem("coll_file_article_01")
	],
	filter: {
		maxSize: 4000000, //in bytes

		allow: { extensions: ['pdf', 'doc', 'docx', 'txt'] },
		// allow: { contentTypes: ['application/pdf'] },

		onInvalid: function (message) {
			if (Meteor.isClient) {
				alert('Onjuist bestandstype of bestandsgrootte.');
			} else {
				console.log('RD: File upload failed');
			}
		}

	}
});


Images = new FS.Collection('images', {
	stores: [
		new FS.Store.FileSystem('sizeNormal', {
			transformWrite: function(fileObj, readStream, writeStream) {	// Scale image down if larger than maximum
				var w2 = 444;		// max pixel width
				var h2 = 888;		// max pixel heigth
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {				// Equal size
							this.stream().pipe(writeStream);
						} else if (r1<r2) {									// Portrait
							if ( h1>h2 )	{
								this.resize(null, h2).stream().pipe(writeStream);
							} else {
								this.stream().pipe(writeStream);
							}
						} else {														// Landscape
							if (w1>w2){
								this.resize(w2).stream().pipe(writeStream);
							} else {
								this.stream().pipe(writeStream);
							}
						}
				  }
				  else{
						console.log('RD: Error on uploading image.');
				  }
				});
			}
		})
	],
  filter: {
    //maxSize: 2000000, 	//in bytes
    allow: {
      contentTypes: ['image/*']
      // extensions: ['jpg', 'jpeg']
    },
    // deny: {
      // extensions: ['png']
    // },
    onInvalid: function (message) {
      if (Meteor.isClient) {
        alert(message);
      } else {
        console.log(message);
      }
    }
  }
});



// carousel //////////////////////////////////////////////////////////////////
ImagesCarousel = new FS.Collection('imagesCarousel', {
	stores: [
		new FS.Store.FileSystem("imagesCarousel", {
			transformWrite: function(fileObj, readStream, writeStream) {
				// var w2 = 900;		// scale and crop
				// var h2 = 400;
				var w2 = 990;		// scale and crop
				var h2 = 450;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {					// Equal size
							this.stream().pipe(writeStream);
						} else if (r1<r2) {										// Portrait
							if (w1==w2)	{
								var y = (h1-h2)/2;
								this.crop(w2,h2,0,y).stream().pipe(writeStream);
							} else {
								var h1s = h1 * w2 / w1;
								var y = (h1s-h2)/2;
								this.resize(w2, h1s).crop(w2,h2,0,y).stream().pipe(writeStream);
							}
						} else {															// Landscape
							if (h1==h2){
								var x = (w1-w2)/2;
								this.crop(w2,h2,x,0).stream().pipe(writeStream);
							} else {
								var w1s = w1 * h2 /h1;
								var x = (w1s-w2)/2;
								this.resize(w1s, h2).crop(w2,h2,x,0).stream().pipe(writeStream);
							}
						}
				  }
				  else {console.log('RD: Error on fileUpload.');}
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});



// slide 540 x 300  //////////////////////////////////////////////////////////////////
ImagesSlide = new FS.Collection('imagesSlide', {
	stores: [
		new FS.Store.FileSystem("imagesSlide", {
			transformWrite: function(fileObj, readStream, writeStream) {
				var w2 = 540;		// scale and crop
				var h2 = 300;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {					// Equal size
							this.stream().pipe(writeStream);
						} else if (r1<r2) {										// Portrait
							if (w1==w2)	{
								var y = (h1-h2)/2;
								this.crop(w2,h2,0,y).stream().pipe(writeStream);
							} else {
								var h1s = h1 * w2 / w1;
								var y = (h1s-h2)/2;
								this.resize(w2, h1s).crop(w2,h2,0,y).stream().pipe(writeStream);
							}
						} else {															// Landscape
							if (h1==h2){
								var x = (w1-w2)/2;
								this.crop(w2,h2,x,0).stream().pipe(writeStream);
							} else {
								var w1s = w1 * h2 /h1;
								var x = (w1s-w2)/2;
								this.resize(w1s, h2).crop(w2,h2,x,0).stream().pipe(writeStream);
							}
						}
				  }
				  else {console.log('RD: Error on fileUpload.');}
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});



// thumb 289 x 300  //////////////////////////////////////////////////////////////////
ImagesThumb = new FS.Collection('imagesThumb', {
	stores: [
		new FS.Store.FileSystem("imagesThumb", {
			transformWrite: function(fileObj, readStream, writeStream) {
				var w2 = 289;		// scale and crop
				var h2 = 300;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {					// Equal size
							this.stream().pipe(writeStream);
						} else if (r1<r2) {										// Portrait
							if (w1==w2)	{
								var y = (h1-h2)/2;
								this.crop(w2,h2,0,y).stream().pipe(writeStream);
							} else {
								var h1s = h1 * w2 / w1;
								var y = (h1s-h2)/2;
								this.resize(w2, h1s).crop(w2,h2,0,y).stream().pipe(writeStream);
							}
						} else {															// Landscape
							if (h1==h2){
								var x = (w1-w2)/2;
								this.crop(w2,h2,x,0).stream().pipe(writeStream);
							} else {
								var w1s = w1 * h2 /h1;
								var x = (w1s-w2)/2;
								this.resize(w1s, h2).crop(w2,h2,x,0).stream().pipe(writeStream);
							}
						}
				  }
				  else {console.log('RD: Error on fileUpload.');}
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});



// photo 250 x 250 round //////////////////////////////////////////////////////////////////
ImagesPhoto = new FS.Collection('imagesPhoto', {
	stores: [
		new FS.Store.FileSystem("imagesPhoto", {
			// path: '~/websites/trefpuntgroen_ehv/public/afbeeldingen/carroussel/normal',
			transformWrite: function(fileObj, readStream, writeStream) {
				var w2 = 250;		// scale and crop
				var h2 = 250;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {					// Equal size
							this.stream().pipe(writeStream);
							// this.matteColor(255,0,255).frame(100,100,40,20).stream().pipe(writeStream); // experiment

						} else if (r1<r2) {										// Portrait
							if (w1==w2)	{
								var y = (h1-h2)/2;
								this.crop(w2,h2,0,y).stream().pipe(writeStream);
							} else {
								var h1s = h1 * w2 / w1;
								var y = (h1s-h2)/2;
								this.resize(w2, h1s).crop(w2,h2,0,y).stream().pipe(writeStream);
							}
						} else {															// Landscape
							if (h1==h2){
								var x = (w1-w2)/2;
								this.crop(w2,h2,x,0).stream().pipe(writeStream);
							} else {
								var w1s = w1 * h2 /h1;
								var x = (w1s-w2)/2;
								this.resize(w1s, h2).crop(w2,h2,x,0).stream().pipe(writeStream);
							}
						}
				  }
				  else {console.log('RD: Error on fileUpload.');}
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});


// customer logo 289 x 150, grey //////////////////////////////////////////////////////////////////
ImagesCustomer = new FS.Collection('imagesCustomer', {
	stores: [
		new FS.Store.FileSystem("imagesCustomer", {
			// path: '~/websites/trefpuntgroen_ehv/public/afbeeldingen/carroussel/normal',
			transformWrite: function(fileObj, readStream, writeStream) {
				var w2 = 289;		// scale and crop
				var h2 = 150;
				var r2 = w2/h2;
				gm(readStream, fileObj.name()).size({bufferStream: true}, function (err, size) {
				  if (!err) {
						var w1 = size.width;
						var h1 = size.height;
						var r1 = w1/h1;
						if ( (h1==h2) && (w1==w2) ) {					// Equal size
							this.stream().pipe(writeStream);
							// this.matteColor(255,0,255).frame(100,100,40,20).stream().pipe(writeStream); // experiment

						} else if (r1<r2) {										// Portrait
							if (w1==w2)	{
								var y = (h1-h2)/2;
								this.crop(w2,h2,0,y).stream().pipe(writeStream);
							} else {
								var h1s = h1 * w2 / w1;
								var y = (h1s-h2)/2;
								this.resize(w2, h1s).crop(w2,h2,0,y).stream().pipe(writeStream);
							}
						} else {															// Landscape
							if (h1==h2){
								var x = (w1-w2)/2;
								this.crop(w2,h2,x,0).stream().pipe(writeStream);
							} else {
								var w1s = w1 * h2 /h1;
								var x = (w1s-w2)/2;
								this.resize(w1s, h2).crop(w2,h2,x,0).stream().pipe(writeStream);
							}
						}
				  }
				  else {console.log('RD: Error on fileUpload.');}
				});
			}
		})
	],
	filter: { allow: { contentTypes: ['image/*'] } }
});





///////////////////////////////////////////////////////////////////////////////////

DB_pix_article.allow({
	insert: function(userId, doc){return true;},
	// download: function(userId, doc){return true;},							// only downloadable if logged in
	download: function(){return true;},														// allways downloadable
	update: function(userId, docs, fields, modifier){return true;},
	remove: function (userId, docs){return true;}});

DB_file_article.allow({
	insert: function(userId, doc){return true;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return true;},
	remove: function (userId, docs){return true;}});

Images.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});



ImagesCarousel.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});

ImagesSlide.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});

ImagesThumb.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});

ImagesPhoto.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});

ImagesCustomer.allow({
	insert: function(userId, doc){return !! userId;},
	download: function(){return true;},
	update: function(userId, docs, fields, modifier){return !! userId;},
	remove: function (userId, docs){return !! userId;}});




	
