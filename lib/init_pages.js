// Pages in Nav and URL
//
// 	urlName			URL															[language]
// 	btnName			Nav button level1 and level2  	[language]
//	pathType		normal, singleSlug, doubleSlug, query
//	template			template in client/pages/
//								contains front and cms template
//								cms template holds cmsTabs


pages = 
	[
		{	urlName: ['introductie', 'introduction'],
			btnName: ['Introductie', 'Introduction'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'home'}  
			]
		},
		// {	urlName: ['programma', 'program'],
		{	urlName: ['programma', 'programme'],
			btnName: ['Programma', 'Programme'],
			level2: [
				// {	urlName: ['symposium', 'symposium'],
				{	urlName: ['conference', 'conference'],
					btnName: ['Conference', 'Conference'],
					pathType: 'singleSlug',
					template: 'symposium'},  
				// {	urlName: ['bewustwording', 'awareness'],
				{	urlName: ['awareness', 'awareness'],
					btnName: ['Awareness', 'Awareness'],
					pathType: 'singleSlug',
					template: 'bewustwording'},  
				{	urlName: ['businessfair', 'businessfair'],
					btnName: ['Business fair', 'Business fair'],
					pathType: 'singleSlug',
					template: 'program'},  
					// template: 'businessfair'},  
				{	urlName: ['satelliet-evenementen', 'satellite-events'],
					btnName: ['Satelliet evenementen', 'Satellite events'],
					pathType: 'singleSlug',
					// template: 'satelliet-evenementen'}  
					template: 'program'}  
			]
		},
		{	urlName: ['nieuws', 'news'],
			btnName: ['Nieuws', 'News'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'news'}  
			]
		},
		{	urlName: ['over-het-evenement', 'about-the-event'],
			btnName: ['Over het evenement', 'About the event'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'singleSlug',
				 template: 'about'}  
			]
		},
		{	urlName: ['contact', 'contact'],
			btnName: ['Contact', 'Contact'],
			level2: [
				{urlName: ['path-nl', 'path-en'],
				 btnName: ['btn name NL', 'btn name EN'],
				 pathType: 'normal',
				 template: 'contact'}  
			]
		}
	]


