Meteor.methods({
	test1: function () {
		console.log('RD: test 1');
	},
	test2: function () {
		console.log('RD: test 2');

		// var blob = new Blob([1,2,3], {type:'text/csv'});
		// [byteArray], {type:'text/csv'}

    var Future = Npm.require('fibers/future');
    var futureResponse = new Future();
    var excel = new Excel('xlsx'); // Create an excel object  for the file you want (xlsx or xls)
    var workbook = excel.createWorkbook(); // Create a workbook (equivalent of an excel file)
		var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook
    worksheet.writeToCell(0,0, 'Conference'); // Example : writing to a cell
    workbook.addSheet('Sheet_Conference', worksheet); // Add the worksheet to the workbook
		// workbook

		// saveAs(workbook, 'test.txt')

		return(workbook);

	},


	downloadExcelFile : function() {
    var Future = Npm.require('fibers/future');
    var futureResponse = new Future();

    var excel = new Excel('xlsx'); // Create an excel object  for the file you want (xlsx or xls)
    var workbook = excel.createWorkbook(); // Create a workbook (equivalent of an excel file)


		// Conference ///////////////////////////////////////////////////////////////////////////////

		var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook
    worksheet.writeToCell(0,0, 'Conference'); // Example : writing to a cell
    worksheet.mergeCells(0,0,0,7); // Example : merging files

    // worksheet.setColumnProperties([ // Example : setting the width of columns in the file
      // { wch: 20 },
      // { wch: 30 }
    // ]);

		var row = 2;
		var reg;
		worksheet.writeToCell(row, 0, 'Dag');
		worksheet.writeToCell(row, 1, 'Tijd');
		worksheet.writeToCell(row, 2, 'Naam');
		worksheet.writeToCell(row, 3, 'Email');
		worksheet.writeToCell(row, 4, 'Telefoon');
		worksheet.writeToCell(row, 5, 'Bedrijf');
		worksheet.writeToCell(row, 6, 'Functie');
		worksheet.writeToCell(row, 7, 'Nationaliteit');
		var array = Registrations.find({track: 'conference'}, {sort: {timeStamp: -1}}).fetch();
		for(var i = 0; i < array.length; i++) {
			row = 4+i;			   // Example : writing multple rows to file
			reg = array[i];
			worksheet.writeToCell(row, 0, reg.timeTextDay);
			worksheet.writeToCell(row, 1, reg.timeTextTime);
			worksheet.writeToCell(row, 2, reg.naam);
			worksheet.writeToCell(row, 3, reg.email);
			worksheet.writeToCell(row, 4, reg.telefoon);
			worksheet.writeToCell(row, 5, reg.bedrijf);
			worksheet.writeToCell(row, 6, reg.functie);
			worksheet.writeToCell(row, 7, reg.nationaliteit);
    }
    workbook.addSheet('Sheet_Conference', worksheet); // Add the worksheet to the workbook



		// Awareness ///////////////////////////////////////////////////////////////////////////////

		var worksheet = excel.createWorksheet(); // Create a worksheet to be added to the workbook
    worksheet.writeToCell(0,0, 'Awareness'); // Example : writing to a cell
    worksheet.mergeCells(0,0,0,5); // Example : merging files
		var row = 2;
		var reg;
		worksheet.writeToCell(row, 0, 'Dag');
		worksheet.writeToCell(row, 1, 'Tijd');
		worksheet.writeToCell(row, 2, 'Naam');
		worksheet.writeToCell(row, 3, 'Email');
		worksheet.writeToCell(row, 4, 'Telefoon');
		worksheet.writeToCell(row, 5, 'Sessie');
		var array = Registrations.find({track: 'awareness'}, {sort: {sessionName: -1}}).fetch();
		for(var i = 0; i < array.length; i++) {
			row = 4+i;			   // Example : writing multple rows to file
			reg = array[i];
			worksheet.writeToCell(row, 0, reg.timeTextDay);
			worksheet.writeToCell(row, 1, reg.timeTextTime);
			worksheet.writeToCell(row, 2, reg.naam);
			worksheet.writeToCell(row, 3, reg.email);
			worksheet.writeToCell(row, 4, reg.telefoon);
			worksheet.writeToCell(row, 5, reg.sessionName);
    }
    workbook.addSheet('Sheet_Awareness', worksheet); // Add the worksheet to the workbook

		


    mkdirp('tmp', Meteor.bindEnvironment(function (err) {
      if (err) {
        console.log('Error creating tmp dir', err);
        futureResponse.throw(err);
      }
      else {
				futureResponse.return('Created tmp dir');
			}
    }));
    return futureResponse.wait();
  }

});
