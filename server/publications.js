// posts

Meteor.publish('posts', function() {
	return Posts.find();
});


// registrations

Meteor.publish('registrations', function() {
	return Registrations.find();
});



// images

Meteor.publish('images', function() {
	return Images.find();
});

Meteor.publish('coll_pix_article', function() {
	return DB_pix_article.find();
});

Meteor.publish('coll_file_article', function() {
	return DB_file_article.find();
});






Meteor.publish('imagesCarousel', function() {
	return ImagesCarousel.find();
});

Meteor.publish('imagesSlide', function() {
	return ImagesSlide.find();
});

Meteor.publish('imagesThumb', function() {
	return ImagesThumb.find();
});

Meteor.publish('imagesPhoto', function() {
	return ImagesPhoto.find();
});

Meteor.publish('imagesCustomer', function() {
	return ImagesCustomer.find();
});
