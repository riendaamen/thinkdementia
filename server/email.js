Meteor.methods({

	mailtest: function () {
		console.log('RD: mailtest.');
	},

	emailContactUs: function (from, subject, body) {
		console.log('RD: sending email...');

		this.unblock();		// Don't wait for result

		// var postURL = process.env.MAILGUN_API_URL + '/' + process.env.MAILGUN_DOMAIN + '/messages';
		var postURL = 'https://api.mailgun.net/v2' + '/' + 'heavymedia.nl' + '/messages';
		var options = {
			auth: "api:" + 'key-4cb557db95932be',
			params: {
				"from":				'info@innovatedementia.eu',
				"to":					['info@innovatedementia.eu'],
				"subject": 		subject,
				"html": 			body
			}
		}
		var onError = function(error, result) {
			if(error) {console.log("Error: " + error)}
		}

		// Send the request
		Meteor.http.post(postURL, options, onError);
		console.log('RD: email sent');



	}
});
