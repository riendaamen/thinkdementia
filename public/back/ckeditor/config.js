/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	// config.toolbarGroups = [
		// { name: 'styles', groups: [ 'format' ] },
		// { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		// { name: 'paragraph',   groups: [ 'list'] },
    // '/',
		// { name: 'links' },
		// { name: 'insert' },
		// { name: 'others' }
	// ];

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'undo', 'clipboard' ] },
		{ name: 'links' },
		{ name: 'insert' },
		'/',
		{ name: 'styles' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list' ] },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] }, // source edit
		{ name: 'tools' }		// fullscreen
	];
	
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
  //config.removeButtons = 'Underline,Subscript,Superscript,Strike,Anchor,Format';
  //config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor';
	// config.removeButtons = 'Underline,Subscript,Superscript,Strike,Anchor,Styles,Table,HorizontalRule';
	config.removeButtons = 'Underline,Subscript,Superscript,Strike,Anchor,Styles,HorizontalRule';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
  
  // RD settings
  config.uiColor = 'hsl(200, 00%, 70%)';
  config.width = '453px';  
  config.stylesSet = [];
	config.skin = 'icy_green_RD_001';
	config.extraPlugins = 'youtube';
  
};






// This is actually the default value.
//config.toolbar_Full =
//[
//    { name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
//    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
//    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
//    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
//    '/',
//    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
//    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
//    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
//    { name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
//    '/',
//    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
//    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
//    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
//];



