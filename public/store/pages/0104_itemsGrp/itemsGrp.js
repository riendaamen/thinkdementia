// itemsGrp  /main 		////////////////////////////////////////////////////////
Template.itemsGrp.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	},
	thumb: function() {
		// Session.set('currentPostGroup', '_pagesGroup');
		Session.set('currentPostGroup', '_groupsGroup');
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.items.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});


// itemsGrp  /main/group 		////////////////////////////////////////////////////////
Template.itemsGrp_slug1.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},
	page: function() {
		
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_groupsGroup';
		var s = nav.param1;
		var query1 = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query1);

		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		// getSEO_fromPost(query);
		return query;
	},
	
	
	
	
	thumb: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = nav.param1;
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.itemsGrp_slug1.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		clientLog ('routerName = ' + routerName );
		// Router.go(routerName + '_slug1', {x: this.slug});
		Router.go(routerName + '_slug2', {x: nav.param1, y: this.slug});
	}
});


// itemsGrp  /main/group/x 		////////////////////////////////////////////////////////
Template.itemsGrp_slug2.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = Session.get('navParam1');
		var s = Session.get('navParam2');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});

Template.itemsGrp_slug2.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');

		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		// Router.go(routerName);																	// go back to /main
		
		var routerName = getPageName(x, y, nav.cmsMode);
		Router.go(routerName + '_slug1', {x: nav.param1});			// go back to /main/group
		
	},
	
	'click #btnBestelformulier': function(e){									// goto bestel-formulier
		e.preventDefault();
		clientLog ('goto bestel form');
		Router.go('bestelformulier', {item: this._id});
	}

	
});



