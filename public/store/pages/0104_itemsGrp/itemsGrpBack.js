// itemsGrp ////////////////////////////////////////////////////

Template.itemsGrpCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsItemsGrpPage', 			name: 'Pagina'},
			{component: 'cmsItemsGrpGroups', 		name: 'Groups'},
			{component: 'cmsItemsGrpProducts', 	name: 'Items'}
		];
	}
});


Template.cmsItemsGrpPage.helpers({
	editTabs: function() {
		// clientLog('slugbase = ' + getPageSlugBase() ); 
		nav.editTabs = [
			{tabName: 'itemText', name: 'Text'},
			{tabName: 'quote', 		name: 'Quote'},
			{tabName: 'seo', 			name: 'SEO'}
		];
		// if ( getPageSlugBase() == 'meubels.path-nl' ){
			// nav.editTabs = [
				// {tabName: 'itemText', name: 'Text'},
				// {tabName: 'seo', 			name: 'SEO'}
			// ];
		// }
	}
});


Template.cmsItemsGrpGroups.helpers({
	editTabs: function() {
		// clientLog('slugbase = ' + getPageSlugBase() ); 
		nav.editTabs = [
			{tabName: 'groupText', 	name: 'Text'},
			{tabName: 'itemPhoto', 	name: 'Photo'},
			{tabName: 'seo', 				name: 'SEO'}
		];
	}
});


Template.cmsItemsGrpProducts.helpers({
	// group: function() {
		// Session.set('currentPostGroup', 'klanten');
	// },
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'itemProduct', 				name: 'Front'},
			{tabName: 'itemPhoto', 					name: 'Photo'},
			{tabName: 'itemSlides', 				name: 'Slides'},
			{tabName: 'itemProductDetail', 	name: 'Detail'},
			{tabName: 'itemVariations', 		name: 'Uitvoeringen'},
			{tabName: 'seo', 								name: 'SEO'}
		];
	}
});



