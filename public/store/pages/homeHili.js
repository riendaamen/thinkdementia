// hili ////////////////////////////////////////////////////////

Template.homeHili.helpers({
		hili: function() {
		var list = this.hiliList;
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			// slide.imageId = list[i];
			slide.id = list[i].hiliItem;
			slide.number = i+1;
			slide.index = i;
			slides.push(slide);
		}
		return slides;
	},
	hiliPreview: function() {
		// var item = Posts.find( {'_id': Session.get('hiliItem')} );
		var item = Posts.find( {'_id': this.id} );
		return item;
	},
	
	hiliUrl: function() {
		var url = '';
		url = url + this.page.split('.')[0];
		if (this.groupSlug !== '_pagesGroup') {
			url = url + '/' + this.groupSlug;
		}
		url = url + '/' + this.slug;
		return url;
	},

	hiliImage: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}

});

