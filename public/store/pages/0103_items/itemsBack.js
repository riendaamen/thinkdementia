
// items ////////////////////////////////////////////////////

Template.itemsCms.helpers({
	cmsTabs: function() {
		nav.cmsTabs = [
			{component: 'cmsItemsPage', 				name: 'Pagina'},
			{component: 'cmsItemsProducts', 		name: 'Items'}
		];
	}
});



Template.cmsItemsPage.helpers({
	editTabs: function() {
		// clientLog('slugbase = ' + getPageSlugBase() ); 
	
		nav.editTabs = [
			{tabName: 'itemText', name: 'Text'},
			{tabName: 'quote', 		name: 'Quote'},
			{tabName: 'seo', 			name: 'SEO'}
		];
		if ( (getPageSlugBase() == 'exposities.path-nl') || (getPageSlugBase() == 'publicaties.path-nl') ){
			nav.editTabs = [
				{tabName: 'quote', 		name: 'Quote'},
				{tabName: 'seo', 			name: 'SEO'}
			];
		}
	}
});


Template.cmsItemsProducts.helpers({
	// group: function() {
		// Session.set('currentPostGroup', 'klanten');
	// },
	editTabs: function() {
		nav.editTabs = [
			{tabName: 'itemProduct', 				name: 'Front'},
			{tabName: 'itemPhoto', 					name: 'Photo'},
			{tabName: 'itemSlides', 				name: 'Slides'},
			{tabName: 'itemProductDetail', 	name: 'Detail'},
			{tabName: 'itemVariations', 		name: 'Uitvoeringen'},
			{tabName: 'seo', 								name: 'SEO'}
		];
		if ( (getPageSlugBase() == 'exposities.path-nl') || (getPageSlugBase() == 'publicaties.path-nl') ){
			nav.editTabs = [
				{tabName: 'itemFront', 			name: 'Front'},
				{tabName: 'itemPhoto', 			name: 'Photo'},
				{tabName: 'itemSlides', 		name: 'Slides'},
				{tabName: 'itemDetail', 		name: 'Detail'},
				{tabName: 'seo', 						name: 'SEO'}
			];
		}		
	}
});

