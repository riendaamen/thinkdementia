// items  /main 		////////////////////////////////////////////////////////

Template.items.helpers({
	pageName: function() {
		return getPageSlugBase().split('.')[0];
	},
	// currentURL: function() {
		// return  Router.current().url;
	// },
	page: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_mainGroup';
		var s = '_main';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	},
	thumb: function() {
		Session.set('currentPostGroup', '_pagesGroup');
		var l = Session.get('language');
		// if ( getPageSlugBase() == 'actueel.agenda' ) {
			// l = -1;
		// }
		var p = getPageSlugBase();
		var g = getPostGroupSlug();
		var item = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return item;
	},
	image: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});

Template.items.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName + '_slug1', {x: this.slug});
	}
});



// items_slug1  /main/x 		////////////////////////////////////////////////////////

Template.items_slug1.helpers({
	item: function() {
		var l = Session.get('language');
		var p = getPageSlugBase();
		var g = '_pagesGroup';
		var s = Session.get('navParam1');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(query);
		return query;
	}
});

Template.items_slug1.events({
	'click .btn': function(e){																// goto link
		e.preventDefault();
		var x = Session.get('navLevel1');
		var y = Session.get('navLevel2');
		var routerName = getPageName(x, y, nav.cmsMode);
		// clientLog ('routerName = ' + routerName );
		Router.go(routerName);
	}
});


// btn bestellen 	////////////////////////////////////////////////////////

Template.items_bestellen.helpers({
	showFlag: function() {
		var show = true;
		var p = getPageSlugBase().split('.')[0];
		if (p=='exposities' || p=='publicaties') {
			show = false;
		}
		return show;
	}
});

Template.items_bestellen.events({
	'click #btnBestelformulier': function(e){									// goto bestel-formulier
		e.preventDefault();
		clientLog ('goto bestel form');
		Router.go('bestelformulier', {item: this._id});
	}
});


