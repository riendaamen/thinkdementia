// klanten ////////////////////////////////////////////////////////

Template.klanten.helpers({
	item: function() {
		var l = Session.get('language');
		var p = 'klanten.path-nl'; 
		var g = '_mainGroup';
		var s = '_main';
		var post = Posts.find({'language': l, 'page': p, 'groupSlug': g, 'slug': s});
		getSEO_fromPost(post);
		return post;
	},

	
	klant: function() {
		// Session.set('currentPostGroup', '_pagesGroup');
		// Session.set('currentPostGroup', 'slider');
		var l = Session.get('cmsLanguage');
		var p = getPageSlugBase();
		// var g = getPostGroupSlug();
		var g = 'klanten';
		var mainPost = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		return mainPost;
	},

	
	
	
	image: function() {
		return ImagesCustomer.find( {'_id': this.image } );
	}

	
	
});

