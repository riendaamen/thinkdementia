
Template.uitvoeringen.helpers({
	variation: function() {
		var list = this.variationList;
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.index = 	i;
			slide.name = 		list[i].name;
			slide.price = 	list[i].price;
			// slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	}
});


