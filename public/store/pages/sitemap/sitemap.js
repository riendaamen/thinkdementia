// sitemap ////////////////////////////////////////////////////////

Template.sitemap.helpers({
	page: function() {
		var list = [];
		var language = Session.get('language');
		for(var i = 0; i < (pages.length); i++) {
			var item = {};
			item.index = 					i;
			item.buttonName = 		pages[i].btnName[language];
			item.myUrl = 					clickLevel1(i);
			list.push(item);
		}
		return list;
	},
	group: function() {
		var l = Session.get('language');
		var p = pages[this.index].urlName[l] + '.path-nl';
		var g = '_groupsGroup';
		var ret = Posts.find( {'page': p, 'language': l, 'groupSlug': g}, {sort: {timeStamp: 1}} );
		return ret;
	},
	itemGrp: function(parentContext) {
		var l = Session.get('language');
		var p = pages[parentContext.index].urlName[l] + '.path-nl';
		var g = this.slug;
		var ret = Posts.find( {'page': p, 'language': l, 'groupSlug': g}, {sort: {timeStamp: 1}} );
		return ret;
	},
	item: function() {
		var l = Session.get('language');
		var p = pages[this.index].urlName[l] + '.path-nl';
		var g = '_pagesGroup';
		var ret = Posts.find( {'page': p, 'language': l, 'groupSlug': g}, {sort: {timeStamp: 1}} );
		return ret;
	}
});





