

// still needs honeypot


// form validation ////////////////////////////////////////////////////////////////

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};


// bestelformulier ////////////////////////////////////////////////////////

Template.bestelformulier.rendered = function(){
	Session.set('notification_name', '');
	Session.set('notification_email', '');
	Session.set('notification_phone', '');
	Session.set('notification_terms', '');
	Session.set('bestelform_variation', 0);
	Session.set('bestelform_amount', 1);
};


Template.bestelformulier.helpers({
	notification_name: function() {
		return Session.get('notification_name');
	},
	notification_email: function() {
		return Session.get('notification_email');
	},
	notification_phone: function() {
		return Session.get('notification_phone');
	},
	notification_terms: function() {
		return Session.get('notification_terms');
	},

	item: function() {
		var query = Posts.find({'_id': nav.param1});
		return query;
	},
	variationOption: function() {
		var list = this.variationList;
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.index = 	i;
			slide.name = 		list[i].name;
			slide.price = 	list[i].price;
			slide.active = (i == Session.get('currentVariation')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	price_A: function() {
		var price = this.variationList[Session.get('bestelform_variation')].price;
		var amount = Session.get('bestelform_amount');
		return (price*amount).toFixed(2);
	},
	price_B: function() {
		var price = this.variationList[Session.get('bestelform_variation')].price;
		var amount = Session.get('bestelform_amount');
		return ((price*amount)+50).toFixed(2);
	}
});


Template.bestelformulier.events ({
	'change #selectVariation': function(event, template){							// select amount
		var num = $('#selectVariation option:selected').data('num');
		Session.set('bestelform_variation', num);
		// clientLog ('select variation = ' + num );
	},
	'change #selectAmount': function(event, template){								// select amount
		var num = $('#selectAmount option:selected').data('num');
		Session.set('bestelform_amount', num);
		// clientLog ('select amount = ' + num );
	},
	
	'click #submit': function(e){																			// form request submit
		e.preventDefault();
		var notification;
		
		var name_valid = ($('#achternaam').val() !== '') ? true : false;
		notification = (name_valid) ? '' : 'Graag je naam invullen.';
		Session.set('notification_name', notification);
		
		var email_valid = (!isValidEmailAddress( $('#email').val() )) ? false : true;
		notification = (email_valid) ? '' : 'Het e-mailadres ontbreekt of klopt niet.';
		Session.set('notification_email', notification);
		
		var phone_valid = ($('#telefoon-mobiel').val() !== '') ? true : false;
		notification = (phone_valid) ? '' : 'Het telefoon-nummer ontbreekt.';
		Session.set('notification_phone', notification);
		
		var terms_valid = ( $('input[name="terms"]:checked').val() == 'accept') ? true : false;
		notification = (terms_valid) ? '' : 'U dient akkoord te gaan met de voorwaarden.';
		Session.set('notification_terms', notification);
		
		
		if (name_valid && email_valid && phone_valid && terms_valid) {					// form is valid
		// if (true) {								// form is valid
			
			var query = Posts.find({'_id': nav.param1}).fetch()[0];
			var variant = query.variationList[Session.get('bestelform_variation')].name;
			var price = query.variationList[Session.get('bestelform_variation')].price;
			var amount = Session.get('bestelform_amount');
			var priceTotal = ((price*amount)+50).toFixed(2);

			
			// var message = '';					// '%0D%0A'  '\r\n' CR en LF for email
			
			// message = message + 'naam product: '											+ query.title 							+ '%0D%0A';
			// message = message + 'product page: '											+ query.page.split('.')[0] 	+ '%0D%0A';
			// if(query.groupSlug !== '_pagesGroup'){
				// message = message + 'product group: '										+ query.groupName						+ '%0D%0A';
			// }
			// message = message + 'uitvoering: '												+ variant			 							+ '%0D%0A';
			// message = message + 'prijs per stuk: '										+ price				 							+ '%0D%0A';
			// message = message + 'aantal: '														+ amount			 							+ '%0D%0A';
			// message = message + 'totaal prijs incl. verzendkosten: '	+ priceTotal								+ '%0D%0A';
			
			// message = message + '%0D%0A';
			// message = message + 'voornaam: '				+ $('#voornaam').val() 				+ '%0D%0A';
			// message = message + 'tussenvoegsel: '		+ $('#tussenvoegsel').val() 	+ '%0D%0A';
			// message = message + 'achternaam: '			+ $('#achternaam').val() 			+ '%0D%0A';
			// message = message + 'straatnaam: '			+ $('#straatnaam').val() 			+ '%0D%0A';
			// message = message + 'huisnummer: '			+ $('#huisnummer').val() 			+ '%0D%0A';
			// message = message + 'postcode: '				+ $('#postcode').val() 				+ '%0D%0A';
			// message = message + 'stad: '						+ $('#stad').val() 						+ '%0D%0A';
			// message = message + 'email: '						+ $('#email').val() 					+ '%0D%0A';
			// message = message + 'telefoon-mobiel: '	+ $('#telefoon-mobiel').val() + '%0D%0A';
			// message = message + 'opmerkingen: '			+ $('#opmerkingen').val() 		+ '%0D%0A';
			// message = message + 'levering: '				+ $('input[name="bestelling"]:checked').val() 		+ '%0D%0A';
			// message = message + '%0D%0A';
			
			
			
			
			
			var m = '';
			var c = "<tr><td style='color:#C7B296; width: 40%;'>"
		
			m = m + "<table border='0' style='width:520px; color: #53473F; font-size: 12px; border-spacing: 2px; padding: 14px; margin: 10px; line-height: 20px; font-family:verdana; border: 0px;'>"; 
			m = m + "<caption style='font-weight: bold; text-align: left; margin-left: 18px;'>Bestelformulier</caption>";
		
			m = m + c + 'naam product' 				+ '</td><td>' + query.title 									+ '</td></tr>';				
			m = m + c + 'product page: '			+ '</td><td>' + query.page.split('.')[0] 			+ '</td></tr>';
			if(query.groupSlug !== '_pagesGroup'){
			m = m + c + 'product group: '			+ '</td><td>' + query.groupName								+ '</td></tr>';
			}
			m = m + c + 'uitvoering: '												+ '</td><td>'+ variant			 	+ '</td></tr>';	
			m = m + c + 'prijs per stuk: '										+ '</td><td>'+ price				 	+ '</td></tr>';
			m = m + c + 'aantal: '														+ '</td><td>'+ amount			 		+ '</td></tr>';
			m = m + c + 'totaal prijs incl. verzendkosten: '	+ '</td><td>'+ priceTotal			+ '</td></tr>';

			m = m + c + '-'																		+ '</td><td>'+ '-'						+ '</td></tr>';
			

			m = m + c + 'voornaam: '				+ '</td></td>' + $('#voornaam').val() 					+ '</td></tr>';
			m = m + c + 'tussenvoegsel: '		+ '</td></td>' + $('#tussenvoegsel').val() 			+ '</td></tr>';
			m = m + c + 'achternaam: '			+ '</td></td>' + $('#achternaam').val() 				+ '</td></tr>';
			m = m + c + 'straatnaam: '			+ '</td></td>' + $('#straatnaam').val() 				+ '</td></tr>';
			m = m + c + 'huisnummer: '			+ '</td></td>' + $('#huisnummer').val() 				+ '</td></tr>';
			m = m + c + 'postcode: '				+ '</td></td>' + $('#postcode').val() 					+ '</td></tr>';
			m = m + c + 'stad: '						+ '</td></td>' + $('#stad').val() 							+ '</td></tr>';
			m = m + c + 'email: '						+ '</td></td>' + $('#email').val() 							+ '</td></tr>';
			m = m + c + 'telefoon-mobiel: '	+ '</td></td>' + $('#telefoon-mobiel').val() 										+ '</td></tr>';
			m = m + c + 'opmerkingen: '			+ '</td></td>' + $('#opmerkingen').val() 												+ '</td></tr>';
			m = m + c + 'levering: '				+ '</td></td>' + $('input[name="bestelling"]:checked').val()  	+ '</td></tr>';
			
			
			m = m + "</table>"
			
			
			
			
			
			
			var email = $('#email').val();

			
			clientLog('send email: ' + m);
			// Meteor.call('mailtest');
			Meteor.call('emailFeedback', email, 'Blok bestelformulier', m);
			
			// var content = '';
			// content = content + "<div class='links' style='width: 100%;'>";
				// content = content + "<h2>Dank je wel voor je interesse.</h2>";
				// content = content + "<p><p>Het bericht is verzonden. Wij nemen binnen 24 uur contact met je op.</p></p>";
				// content = content + "<p>&nbsp</p>";
			// content = content + "</div>";
			// $("#submit").colorbox({html: content, width:"600px", transition: "none"});
			
			// clientLog('radio = ' + $('input[name="bestelling"]:checked').val() );
			// clientLog('akoord = ' + $('input[name="terms"]:checked').val() );
			
			Session.set('notification_name', '');
			Session.set('notification_email', '');
			Session.set('notification_phone', '');
			Session.set('notification_terms', '');
			
			$('#voornaam').val('');
			$('#tussenvoegsel').val('');
			$('#achternaam').val('');
			$('#straatnaam').val('');
			$('#huisnummer').val('');
			$('#postcode').val('');
			$('#stad').val('');
			$('#email').val('');
			$('#telefoon-mobiel').val('');
			$('#opmerkingen').val(''); 		
			
			Router.go('bedankt');
			
		}
	}

});



	

