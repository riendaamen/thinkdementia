// maurice //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_maurice.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_maurice.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'mauriceName':				$('#input_mauriceName').val(),
				'mauriceMobile':			$('#input_mauriceMobile').val(),
				'mauriceDescr':				$('#input_mauriceDescr').val()
			}});
			Session.set('edited', false);
		}
	}
});



// anke //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_anke.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_anke.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'ankeName':				$('#input_ankeName').val(),
				'ankeMobile':			$('#input_ankeMobile').val(),
				'ankeDescr':			$('#input_ankeDescr').val()
			}});
			Session.set('edited', false);
		}
	}
});



// werkwijze //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_werkwijze.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_werkwijze.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'homeTextL':			CKEDITOR.instances.input_homeTextL.getData(),
				'homeTextR':			CKEDITOR.instances.input_homeTextR.getData()
			}});
			Session.set('edited', false);
		}
	}
});



// werkvloer //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_werkvloer.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_werkvloer.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				// 'werkvloerTextL':			CKEDITOR.instances.input_textL.getData(),
				// 'werkvloerTextR':			CKEDITOR.instances.input_textR.getData()

				'footerTitle':	$('#input_title').val(),
				'textL':				CKEDITOR.instances.input_textL.getData(),
				'textR':				CKEDITOR.instances.input_textR.getData()
			}});
			Session.set('edited', false);
		}
	}
});




