// contact text //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_contactText.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_contactText.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				//'title':				$('#input_title').val()
				'title':			CKEDITOR.instances.input_title.getData()
				
			}});
			Session.set('edited', false);
		}
	}
});



// contact photo //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_contactPhoto.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_contactPhoto.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'mauriceName':				$('#input_mauriceName').val(),
				'mauriceMobile':			$('#input_mauriceMobile').val(),
				'mauriceDescr':				$('#input_mauriceDescr').val()
			}});
			Session.set('edited', false);
		}
	}
});


