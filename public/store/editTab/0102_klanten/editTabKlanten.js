// klanten  //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_klantTitle.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_klantTitle.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':			$('#input_title').val(),
				'link':				$('#input_link').val()
			}});
			Session.set('edited', false);
		}
	}
});




Template.cmsEdit_klantArchitecten.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_klantArchitecten.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'klantArchitecten':			CKEDITOR.instances.input_klantArchitecten.getData(),
			}});
			Session.set('edited', false);
		}
	}
});



Template.cmsEdit_klantOntwerpers.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_klantOntwerpers.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'klantOntwerpers':			CKEDITOR.instances.input_klantOntwerpers.getData(),
			}});
			Session.set('edited', false);
		}
	}
});


