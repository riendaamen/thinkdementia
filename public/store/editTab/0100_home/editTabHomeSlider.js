// home sliderTitle //////////////////////////////////////////////////////////////////////////

Template.cmsEdit_homeSlider.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_homeSlider.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':							$('#input_sliderTitle').val(),
				'sliderLinkText':			$('#input_sliderLinkText').val(),
				'sliderLinkUrl':			$('#input_sliderLinkUrl').val()
			}});
			Session.set('edited', false);
		}
	}
});








// home sliderTitle //////////////////////////////////////////////////////////////////////////

// Template.cmsEdit_sliderTitle.helpers({
	// updateStatus: function() {
		// var classActive = (Session.get('edited')) ? '' : 'disabled';
		// return {buttonId: this._id, title: 'Update', classActive: classActive};
	// }
// });

// Template.cmsEdit_sliderTitle.events({	
	// 'click #update': function(e) {															// Update item
		// e.preventDefault();
		// if (Session.get('edited')) {
			// Posts.update( {'_id': this.buttonId}, {$set: {
				// 'title':							$('#input_sliderTitle').val(),
				// 'sliderLinkText':			$('#input_sliderLinkText').val(),
				// 'sliderLinkUrl':			$('#input_sliderLinkUrl').val()
			// }});
			// Session.set('edited', false);
		// }
	// }
// });


