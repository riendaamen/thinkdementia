// home highlights //////////////////////////////////////////////////////////////////////////

Session.set('hiliPage', 	'exposities');
Session.set('hiliGroup', 	0);
Session.set('hiliItem', 	0);
Session.set('currentHili', 0);
Session.set('currentPost', 0);

Template.cmsEdit_highlight.rendered = function(){
	var p = Session.get('currentPost');
	var post = Posts.find( {_id: p} ).fetch()[0];
	var i = Session.get('currentHili');
	var list = post.hiliList;
	var t = list[i];
	Session.set('hiliPage', 	t.hiliPage);
	Session.set('hiliGroup', 	t.hiliGroup);
	Session.set('hiliItem', 	t.hiliItem);
};

Template.cmsEdit_highlight.helpers({
	hiliShow: function() {
		Session.set('currentPost', this._id);
		var post = Posts.find( {_id: this._id} ).fetch()[0];
		var list = post.hiliList;
		
		
		clientLog('hilishow' );

		
		
		
		
		return ( list.length > 0 ) ? true : false;
	},
	hiliNav: function() {
		var list = this.hiliList;
		if (Session.get('currentHili') > (list.length-1)) {
			Session.set('currentHili', list.length-1);
		}
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.imageId = list[i];
			slide.number = i+1;
			slide.index = i;
			slide.active = (i == Session.get('currentHili')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	hili: function() {
		return [this.hiliList[Session.get('currentHili')]];
	},
	hiliPage: function() {	
		var mArray = [];
		mArray.push({ num: 	0, title: 'exposities' });
		mArray.push({ num: 	1, title: 'publicaties' });
		mArray.push({ num:  2, title: 'interieur' });
		mArray.push({ num:  3, title: 'meubels' });
		mArray.push({ num: 	4, title: 'tuinhaard' });
		mArray.push({ num:  5, title: "verandas" });
		mArray.push({ num:  6, title: 'kunst-openbare-ruimte' });
		return mArray;
	},
	hiliPageSel: function() {												
		return Session.equals('hiliPage', this.title) ? 'active' : '';
	},
	hiliGroup: function() {	
		var l = Session.get('cmsLanguage');
		var p = Session.get('hiliPage') + '.path-nl';  // en/nl--------------------
		var g = '_groupsGroup';
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {timeStamp: 1}} );
		if (query.fetch().length == 0){
			Session.set('hiliGroup', '_pagesGroup');
		}
		
		return query;
	},
	hiliGroupSel: function() {												
		return Session.equals('hiliGroup', this.slug) ? 'active' : '';
	},
	hiliItem: function() {	
		var l = Session.get('cmsLanguage');
		var p = Session.get('hiliPage') + '.path-nl';  // en/nl-----------------------
		var g = Session.get('hiliGroup');
		var query = Posts.find({'language': l, 'page': p, 'groupSlug': g}, {sort: {order: 1}} );
		if (query.fetch().length == 0){
			// Session.set('hiliItem', '');
		}
		return query;
	},
	hiliItemSel: function() {												
		return Session.equals('hiliItem', this._id) ? 'active' : '';
	},
	hiliPreview: function() {
		var item = Posts.find( {'_id': Session.get('hiliItem')} );
		return item;
	},
	hiliUrl: function() {
		var url = '';
		url = url + this.page.split('.')[0];
		if (this.groupSlug !== '_pagesGroup') {
			url = url + '/' + this.groupSlug;
		}
		url = url + '/' + this.slug;
		return url;
	},
	
	
	
	
	hiliImage: function() {
		return ImagesThumb.find( {'_id': this.image_01 } );
	}
});




Template.cmsEdit_highlight.events({
	'click .addQuote': function() {														// add hili
		var postId = this._id;
		var list = this.hiliList;
		list.push({
			'hiliPage':			0,
			'hiliGroup':		0,
			'hiliItem':			0
		});
		Posts.update( {'_id': postId}, {$set: {
			'hiliList':	list
		}});
		Session.set('currentHili', list.length-1);
		Session.set('hiliPage', 	0);
		Session.set('hiliGroup', 	0);
		Session.set('hiliItem', 	0);
  },
	
	'click .btnSlideNum': function(){													// select hili
		Session.set('currentHili', this.index);
		// clientLog('select hili ' + this.index);
		var p = Session.get('currentPost');
		var post = Posts.find( {_id: p} ).fetch()[0];
		var i = Session.get('currentHili');
		var list = post.hiliList;
		var t = list[i];
		Session.set('hiliPage', 	t.hiliPage);
		Session.set('hiliGroup', 	t.hiliGroup);
		Session.set('hiliItem', 	t.hiliItem);
		// clientLog('select hili, item = ' + t.hiliPage);
	},
	
	'click .btnDelSlide': function() {												// delete hili
		var r=confirm("Delete this quote?");
		if (r==true){
			var list = this.hiliList;
			list.splice(Session.get('currentHili'), 1);
			this.hiliList = list;
			Posts.update( {'_id': this._id}, {$set: {
				'hiliList':	list
			}});
			Session.set('currentHili', list.length-1);
		}
	},
	'click .hiliPageBtn': function(){											// Select page
		Session.set('hiliPage', this.title);
		clientLog('select hili page ' + this.title);
		var p = Session.get('currentPost');
		// clientLog('select hili page id ' + p);
		var post = Posts.find( {_id: p} ).fetch()[0];
		var i = Session.get('currentHili');
		var list = post.hiliList;
		list[i].hiliPage = this.title;

		list[i].hiliGroup = 0;
		list[i].hiliItem = 0;
		Session.set('hiliGroup', 	0);
		Session.set('hiliItem', 	0);
		
		// clientLog('list =' + list);
		Posts.update( {'_id': p}, {$set: {
			'hiliList':	list
		}});
	},
	
	'click .hiliGroupBtn': function(){										// Select group
		Session.set('hiliGroup', this.slug);
		// clientLog('select hili group ' + this.slug);
		var p = Session.get('currentPost');
		// clientLog('select hili page id ' + p);
		var post = Posts.find( {_id: p} ).fetch()[0];
		var i = Session.get('currentHili');
		var list = post.hiliList;
		list[i].hiliGroup = this.slug;
		
		list[i].hiliItem = 0;
		Session.set('hiliItem', 	0);

		// clientLog('list =' + list);
		Posts.update( {'_id': p}, {$set: {
			'hiliList':	list
		}});
	},
	
	'click .hiliItemBtn': function(){											// Select item
		Session.set('hiliItem', this._id);
		// clientLog('select hili item ' + this._id);
		var p = Session.get('currentPost');
		// clientLog('select hili page id ' + p);
		var post = Posts.find( {_id: p} ).fetch()[0];
		var i = Session.get('currentHili');
		var list = post.hiliList;
		list[i].hiliItem = this._id;
		// clientLog('list =' + list);
		Posts.update( {'_id': p}, {$set: {
			'hiliList':	list
		}});
	}
});




