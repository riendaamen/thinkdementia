
// item front (exposities en publicaties) //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_itemFront.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_itemFront.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'dateText':		$('#input_dateText').val(),
				'title':			$('#input_title').val(),
				'subTitle':		$('#input_subTitle').val()
			}});
			Session.set('edited', false);
		}
	}
});


// item front (product) //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_itemProduct.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_itemProduct.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':			$('#input_title').val(),
				'subTitle':		$('#input_subTitle').val()
			}});
			Session.set('edited', false);
		}
	}
});


// item detail (exposities en publicaties) //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_itemDetail.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_itemDetail.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			var vis = (document.getElementById('input_show').checked) ? 'checked' : '';
			Posts.update( {'_id': this.buttonId}, {$set: {
				'detailShow':		vis, 
				'detailL':			CKEDITOR.instances.input_textL.getData(),
				'detailR':			CKEDITOR.instances.input_textR.getData()
			}});
			Session.set('edited', false);
		}
	}
});


// item detail (product) //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_itemProductDetail.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_itemProductDetail.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'detailL':			CKEDITOR.instances.input_textL.getData(),
				'detailR':			CKEDITOR.instances.input_textR.getData()
			}});
			Session.set('edited', false);
		}
	}
});
