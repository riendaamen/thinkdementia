// item text //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_itemText.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_itemText.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				'footerTitle':	$('#input_title').val(),
				'textL':				CKEDITOR.instances.input_homeTextL.getData(),
				'textR':				CKEDITOR.instances.input_homeTextR.getData()
			}});
			Session.set('edited', false);
		}
	}
});



// group text //////////////////////////////////////////////////////////////////////////
Template.cmsEdit_groupText.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});

Template.cmsEdit_groupText.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			Posts.update( {'_id': this.buttonId}, {$set: {
				// 'title':			$('#input_title').val(),
				'groupName':			$('#input_title').val(),
				'subTitle':		$('#input_subTitle').val()
			}});
			Session.set('edited', false);
		}
	}
});

