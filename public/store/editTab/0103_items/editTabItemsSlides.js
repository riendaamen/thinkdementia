
// item slides //////////////////////////////////////////////////////////////////////////

Session.set('currentSlide', 0);

Template.cmsEdit_itemSlides.helpers({
	slideList: function() {
		var list = this.slideList;
		if (Session.get('currentSlide') > (list.length-1)) {
			Session.set('currentSlide', (list.length-1));
		}
		var slides = [];
		for (var i = 0; i < list.length; i++) {
			var slide = [];
			slide.imageId = list[i];
			slide.number = i+1;
			slide.index = i;
			slide.active = (i == Session.get('currentSlide')) ? 'active' : '';
			slides.push(slide);
		}
		return slides;
	},
	slidesShow: function() {
		var list = this.slideList;
		return (list.length > 0) ? true : false;
	},
	image: function() {
		return ImagesSlide.find( {'_id': this.slideList[Session.get('currentSlide')] } );
	}
});




Template.cmsEdit_itemSlides.events({	
	'click .btnSlideNum': function(){											// select slide
		Session.set('currentSlide', this.index);
	},
  'change #fileUploader': function(event, template) {		// add image
    var files = event.target.files;
		var postId = this._id;
		var list = this.slideList;
    for (var i = 0, ln = files.length; i < ln; i++) {
      ImagesSlide.insert(files[i], function (err, fileObj) {
				list.push(fileObj._id);
				Posts.update( {'_id': postId}, {$set: {
					//'image_01':		fileObj._id,								-- diturbs photo thumb
					'slideList':	list
				}});
				Session.set('currentSlide', list.length);
      });
    }
  },
	'click .btnDelSlide': function() {
		var r=confirm("Delete this image?");
		if (r==true){
			// clientLog('delete slide a = ' + this.slideList);
			var list = this.slideList;
			ImagesSlide.remove( {'_id': list[Session.get('currentSlide')] } );
			list.splice(Session.get('currentSlide'), 1);
			this.slideList = list;
			Posts.update( {'_id': this._id}, {$set: {
				'slideList':	list
			}});
			Session.set('currentSlide', list.length-1);
		}
	}
});




