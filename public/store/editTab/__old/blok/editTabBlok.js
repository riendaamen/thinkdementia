
Template.cmsEdit_blok.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: '> Update Blok', classActive: classActive};
	}
});


Template.cmsEdit_blok.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		// clientLog('update SEO from EditTab, id = ' + this.buttonId);
		if (Session.get('edited')) {
			
			
			Posts.update( {'_id': this.buttonId}, {$set: {
				'block1_title':			$('#input_block1_title').val(),
				'block1_text':			$('#input_block1_text').val(),
				'block1_link':			$('#input_block1_link').val(),
				'block1_linktext':	$('#input_block1_linktext').val()
			}});
			
			
			Session.set('edited', false);
		}
	}
});


