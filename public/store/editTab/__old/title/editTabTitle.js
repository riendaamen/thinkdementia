
Template.cmsEdit_title.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: '> Update title', classActive: classActive};
	}
});


Template.cmsEdit_title.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		if (Session.get('edited')) {
			
			
			Posts.update( {'_id': this.buttonId}, {$set: {
				'title':						$('#input_title').val()
			}});
			
			
			Session.set('edited', false);
		}
	}
});


