
Template.cmsEdit_image.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: '> Update text', classActive: classActive};
	}
});


Template.cmsEdit_image.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		// clientLog('update SEO from EditTab, id = ' + this.buttonId);
		if (Session.get('edited')) {
			
			
			Posts.update( {'_id': this.buttonId}, {$set: {
				'img1_text':						$('#input_img1_text').val(),
				'img1_link':						$('#input_img1_link').val()
			}});
			
			
			Session.set('edited', false);
		}
	}
});


	
// upload /////////////////////////////////////////////////////////////

Template.cmsEdit_image_upload.helpers({
	image: function() {
		return Images.find( {'_id': this.image1 } );
	}
});

Template.cmsEdit_image_upload.events({
  'change #fileUploader': function(event, template) {
    var files = event.target.files;
		var postId = this._id;
    for (var i = 0, ln = files.length; i < ln; i++) {
      Images.insert(files[i], function (err, fileObj) {
				Posts.update( {'_id': postId}, {$set: {
					'image1':	fileObj._id
				}});
      });
    }
  },
	'click .red': function() {
		// var r=confirm("Delete this image?");
		// if (r==true){
			Images.remove( {_id:this._id} );
		// }
	}
});

