
Template.cmsEdit_seo.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});


Template.cmsEdit_seo.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		// clientLog('update SEO from EditTab, id = ' + this.buttonId);
		if (Session.get('edited')) {
			
			
			Posts.update( {'_id': this.buttonId}, {$set: {
				'seoTitle':						$('#input_seoTitle').val(),
				'seoDescr':						$('#input_seoDescr').val()
			}});
			
			
			Session.set('edited', false);
		}
	}
});


