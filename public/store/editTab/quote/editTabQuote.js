
Template.cmsEdit_quote.helpers({
	updateStatus: function() {
		var classActive = (Session.get('edited')) ? '' : 'disabled';
		return {buttonId: this._id, title: 'Update', classActive: classActive};
	}
});


Template.cmsEdit_quote.events({	
	'click #update': function(e) {															// Update item
		e.preventDefault();
		// clientLog('update SEO from EditTab, id = ' + this.buttonId);
		if (Session.get('edited')) {
			
			
				Posts.update( {'_id': this.buttonId}, {$set: {
					// 'quoteSentence':		$('#input_quoteSentence').val(),
					'quoteSentence':		CKEDITOR.instances.input_quoteSentence.getData(),
					
					
					'quotePerson':			$('#input_quotePerson').val()
				}});
			
			
			Session.set('edited', false);
		}
	}
});


